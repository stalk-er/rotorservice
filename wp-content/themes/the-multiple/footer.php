<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package the-multiple
 */

?>
	</div><!-- #content -->
	<footer id="colophon" class="site-footer wow slideInUp" data-wow-delay="1s">
	   <div class="footer-wrap">
	       <div class="bt-container ">
		       <div class="site-footer-info">
		            <div id="the-multiple-footer-three">
	                  <?php
	                  if(is_active_sidebar('the-multiple-footer-one')){
	                        dynamic_sidebar('the-multiple-footer-one');
	                  }
	                    ?>
	                </div>
	                <div id="the-multiple-footer-one">
	                  <?php
	                  if(is_active_sidebar('the-multiple-footer-two')){
	                        dynamic_sidebar('the-multiple-footer-two');
	                  }
	                    ?>
	                </div>
	                <div id="the-multiple-footer-two">
	                  <?php
	                  if(is_active_sidebar('the-multiple-footer-three')){
	                        dynamic_sidebar('the-multiple-footer-three');
	                  }
	                    ?>
	                </div>
	                <div id="the-multiple-footer-four">
	                  <?php
	                  if(is_active_sidebar('the-multiple-footer-four')){
	                        dynamic_sidebar('the-multiple-footer-four');
	                  }
	                    ?>
	                </div>
			   </div>
			   
		   </div><!-- .site-info -->
	    </div>
	    <div class="copyright">
	        <div class="bt-container">
	            <div class="footer_btm_left">
	                <?php
	                 $the_multiple_footer_copyright_text = get_theme_mod('the_multiple_footer_setting_cpy_text');
                      ?><a href="<?php echo esc_url('http://buzthemes.com/wordpress_themes/the-multiple/' ); ?>"><?php printf( esc_html__( 'the-multiple', 'the-multiple' ) ); ?></a><?php echo esc_html__( '  |  Powered by WordPress' , 'the-multiple' );?>
	            </div>
	            <div class="footer_social_icon_front_footer">
	                     <?php 
	                     $the_multiple_footer_social_link = (get_theme_mod('the_multiple_footer_social_section_option','no') == 'yes');
	                     if($the_multiple_footer_social_link){
	                       do_action('the_multiple_footer_social_link_action');
	                     } ?>
	            </div>
	            
	            <div id="ed-top"><i class="fa fa-arrow-up"></i></div>

            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
