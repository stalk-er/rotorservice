<?php
/**
 * the-multiple functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package the-multiple
 */

if ( ! function_exists( 'the_multiple_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function the_multiple_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on the-multiple, use a find and replace
	 * to change 'the-multiple' to the name of your theme in all the template files.
	 */
    load_theme_textdomain( 'the-multiple', get_template_directory() . '/languages' );
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	
	add_theme_support( 'post-thumbnails' );
	add_image_size('the-multiple-slider-image', 2000, 1000, true);
	add_image_size('the-multiple-about-image', 700, 700, true);
	add_image_size('the-multiple-team-thumb', 600, 600, true);
	add_image_size('the-multiple-testimonial-thumb', 400, 400, true);
	add_image_size('the-multiple-latest-news-thumb', 600, 500, true);
	add_image_size('the-multiple-testimonial-thumb-image', 400, 400, true);
	add_image_size('the-multiple-blog-image', 600, 500, true);
	add_image_size('the-multiple-client-thumb', 200, 100, true);
	add_image_size('the-multiple-archive-image', 640, 585, true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'the-multiple' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'the_multiple_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 80,
		'width'       => 262,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'the_multiple_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function the_multiple_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'the-multiple' ),
		'id'            => 'the-multiple-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'the-multiple' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar', 'the-multiple' ),
		'id'            => 'the-multiple-sidebar-right',
		'description'   => esc_html__( 'Add widgets here.', 'the-multiple' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Left Sidebar', 'the-multiple' ),
		'id'            => 'the-multiple-sidebar-left',
		'description'   => esc_html__( 'Add widgets here.', 'the-multiple' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
        'name' => esc_html__('Footer One','the-multiple'),
        'id' => 'the-multiple-footer-one',
        'description' => esc_html__('Appears in the buttom of footer area','the-multiple'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => esc_html__('Footer Two','the-multiple'),
        'id' => 'the-multiple-footer-two',
        'description' => esc_html__('Appears in the buttom of footer area','the-multiple'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => esc_html__('Footer Three','the-multiple'),
        'id' => 'the-multiple-footer-three',
        'description' => esc_html__('Appears in the buttom of footer area','the-multiple'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => esc_html__('Footer Four','the-multiple'),
        'id' => 'the-multiple-footer-four',
        'description' => esc_html__('Appears in the buttom of footer area','the-multiple'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

}
add_action( 'widgets_init', 'the_multiple_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function the_multiple_scripts() {
    
    $the_multiple_font_query_args = array('family' => 'Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Open+Sans');
    wp_enqueue_style('the-multiple-google-fonts', add_query_arg($the_multiple_font_query_args, "//fonts.googleapis.com/css"));
    wp_enqueue_style('owl-carousel',get_template_directory_uri().'/js/owl-carousel/owl.carousel.css');
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/library/font-awesome/css/font-awesome.min.css', array(), '4.6.3' );
    wp_enqueue_style('animate',get_template_directory_uri().'/js/animate/animate.css');
    wp_enqueue_style( 'the-multiple-style', get_stylesheet_uri() );
	wp_enqueue_style('the-multiple-responsive',get_template_directory_uri().'/responsive.css');

	wp_enqueue_script('the-multiple-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script('jquery-counterup', get_template_directory_uri() . '/library/counterup/js/jquery.counterup.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script('jquery-counterup-min', get_template_directory_uri() . '/library/counterup/js/jquery.counterup.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script('owl-carousel',get_template_directory_uri().'/js/owl-carousel/owl.carousel.js',array('jquery'));
	wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.js', array('jquery'), '1.1.3', true );
	wp_enqueue_script('the-multiple-custom',get_template_directory_uri().'/js/custom.js', array('jquery'), '',true);
	 
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
}
add_action( 'wp_enqueue_scripts', 'the_multiple_scripts' );

function the_multiple_content_width() {
$GLOBALS['content_width'] = apply_filters( 'the_multiple_content_width', 640 );
}
add_action( 'after_setup_theme', 'the_multiple_content_width', 0 );
 
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/** Recent Post Widget **/
require get_template_directory() . '/inc/widgets/widgets-recent-post.php';

/** Widget Fields **/
require get_template_directory() . '/inc/widgets/widgets-field.php';
/**

 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */

require get_template_directory() . '/inc/customizer.php';
/**
 * Load Custom functions file.
 */

require get_template_directory() . '/inc/the-multiple-functions.php';
/**
 * Load Custom Customizer file.
 */

require get_template_directory() . '/inc/the-multiple-customizer.php';

/** Metaboxe **/

require get_template_directory() . '/inc/the-multiple-metabox.php';

/**
 * Dynamic Styles additions.
 */
require get_template_directory() . '/inc/style.php';

/** Customizer Classes **/
require get_template_directory() . '/inc/customizer-classes.php';


