jQuery(document).ready(function($){
   /*main slider*/
 $('.main-slider').owlCarousel({
        animateOut: 'bounceOutRight',
        animateIn: 'bounceInLeft',
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        items:1,
        loop:true,
        dots: false,
        mouseDrag: false,
        nav: true,
        navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    })

 /*team slider*/
 $('.team-slider').owlCarousel({
        autoplay: true,
        loop: true,
        margin: 25,
        dots: true,
        responsiveClass: true,
        autoplay: true,
        nav: false,
        navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1,
            },
            540: {
                items: 2,
            },
            768: {
                items: 3,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 3,
            }
        }
    });
    
/*team slider*/
$('.affiliation-slider').owlCarousel({
        loop: true,
        margin: 5,
        responsiveClass: true,
        dots: false,
        autoplay: true,
        responsive: {
            0: {
                items: 2,
            },
            640: {
                items: 3,
            },
            768: {
                items: 5,
            },
            992: {
                items: 6,
            },
            1200: {
                items: 7,
            }
        }
    });
    
 /* Testimonial Carousel */
    $('.testimonial').owlCarousel({
        animateOut: 'bounceOutRight',
        animateIn: 'bounceInRight',
        loop: true,
        responsiveClass: true,
        autoplay: true,
        navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 1,
            },
            992: {
                items: 1,
            },
            1200: {
                items: 1,
            }
        }
    });
    
   //Search Box Toogle
    $('.header-search .fa-search').click(function(){
        $('.header-search .search-form').slideToggle();
    });

    // Scroll Top  
    $('#ed-top').css('right',-65);
    $(window).scroll(function(){
        if($(this).scrollTop() > 300){
            $('#ed-top').css('right',0);
        }else{
            $('#ed-top').css('right',-65);
        }
    });

    $("#ed-top").click(function(){
        $('html,body').animate({scrollTop:0},600);
    });

    
    /*wow js file*/
    new WOW().init();
    
    //Navigation Icon
    $(".icon").click(function(){
        $(".menu").toggleClass("open");
    });

    //Counter
    $('.bt-achievements .number').counterUp({
        delay: 20,
        time: 2000
    });


    // According
    $('.accordion a').click(function(j) {
        var dropDown = $(this).closest('li').find('p');

        $(this).closest('.accordion').find('p').not(dropDown).slideUp();

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).closest('.accordion').find('a.active').removeClass('active');
            $(this).addClass('active');
        }

        dropDown.stop(false, true).slideToggle();

        j.preventDefault();
    });
});