<?php
/**
 *Counter Section 
*/
if(get_theme_mod('the_multiple_homepage_setting_counter_section_option','no') == 'yes'):
  $first_counter_icon = get_theme_mod('first_counter_icon');
  $first_counter_title = get_theme_mod('first_counter_title');
  $first_counter_number = get_theme_mod('first_counter_number');
        
  $second_counter_icon = get_theme_mod('second_counter_icon');
  $second_counter_title = get_theme_mod('second_counter_title');
  $second_counter_number = get_theme_mod('second_counter_number');
          
  $third_counter_icon = get_theme_mod('third_counter_icon');
  $third_counter_title = get_theme_mod('third_counter_title');
  $third_counter_number = get_theme_mod('third_counter_number');
  $counterimage = get_theme_mod('the_multiple_counter_bkgimage','');
  ?>
      <section id="counter-section" class="bt-achievements the-multiple-counter" style="background-image: url('<?php echo esc_url($counterimage); ?>');">
           <div class="bt-container">
                <div class="the-multiple-counter-wrapper">
                      <div class="counter-one wow fadeInUp"  data-wow-delay="0.4s">
                          <?php if($first_counter_icon || $first_counter_title || $first_counter_number){ ?>
                          <div class="col" id="first">
                              <p class="number">
                                <?php if($first_counter_number){ ?><?php echo absint($first_counter_number); ?><?php } ?>
                              </p>
                                <?php if($first_counter_icon){ ?>
                                <div class="icon-holder">
                                    <i class="fa <?php echo esc_attr($first_counter_icon); ?>"></i>
                                </div>
                                <?php } ?>
                                <?php if($first_counter_title){ ?>
                                <span class="title">
                                  <?php echo esc_html($first_counter_title); ?>
                                </span>
                                  <?php } ?>
                          </div>
                              <?php } ?>
                      </div>
                      <div class="counter-two wow fadeInUp"  data-wow-delay="0.8s">
                          <?php if($second_counter_icon || $second_counter_title || $second_counter_number){ ?>
                          <div class="col" id="second">
                              <p class="number">
                                <?php if($second_counter_number){ ?><?php echo absint($second_counter_number); ?><?php } ?>
                              </p>
                              <?php if($second_counter_icon){ ?>
                              <div class="icon-holder">
                                 <i class="fa <?php echo esc_attr($second_counter_icon); ?>"></i>
                              </div>
                              <?php } ?>
                              <?php if($second_counter_title){ ?>
                              <span class="title">
                                  <?php echo esc_html($second_counter_title); ?>
                               </span>
                                <?php } ?>
                            </div>
                            <?php } ?> 
                      </div>
                      <div class="counter-two wow fadeInUp"  data-wow-delay="1.2s">
                         <?php if($third_counter_icon || $third_counter_title || $third_counter_number){ ?>
                          <div class="col" id="third">
                                 <p class="number">
                                   <?php if($third_counter_number){ ?><?php echo absint($third_counter_number); ?><?php } ?>
                                 </p>
                                <?php if($third_counter_icon){ ?>
                                  <div class="icon-holder">
                                      <i class="fa <?php echo esc_attr($third_counter_icon); ?>"></i>
                                  </div>
                                    <?php } ?>
                                    <?php if($third_counter_title){ ?>
                                  <span class="title">
                                    <?php echo esc_html($third_counter_title); ?>
                                  </span>
                              <?php } ?>
                          </div>
                         <?php } ?>  
                      </div>
                 </div>
            </div>
      </section>
    <?php
endif;
         ?>