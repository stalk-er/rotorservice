<?php
/**
 * Contact Us Section 
*/
  if (get_theme_mod('the_multiple_contact_us_option','no')=='yes') {
      $post_id = get_theme_mod('the_multiple_contact_us_post');
      $contact_us_icon = get_theme_mod('contact_us_icon');
      if(!empty($post_id)):
              ?>
          <section id="contact-us-section" class="the-multiple-contact-us-section">
              <div class="bt-container">
                    <?php 
                    $service_args  = array('post_type'=>'post', 'p' => $post_id, 'post_status' => 'publish','posts_per_page'=>1);
                    $service_query = new WP_Query($service_args);
                    if ($service_query->have_posts()):
                        while ($service_query->have_posts()):
                            $service_query->the_post();
                            ?>
                            <div class="contact-us-content-wrap wow fadeInUp" data-wow-delay="0.8s">
                                 <div id="contact-us-post" class="title-wrap" >
                                    <h2 class="section-title"><?php the_title(); ?></h2>
                                 </div>
                                 <div class="contact-us-post-des">
                                    <div id="contact-us-post-post" class="contact-us-post-post" >
                                      <?php echo esc_attr(wp_trim_words(get_the_content(),100,'&hellip;')); ?>
                                    </div>
                                 </div>
                                 <div class="the-multiple-mail-link wow fadeInUp" data-wow-delay="0.8s">
                                     <a href="<?php the_permalink(); ?>" class="btn"><?php echo esc_html(get_theme_mod('the_multiple_contact_us_section_link',esc_html__('Support@buzthemes.com','the-multiple'))); ?></a>
                                     <a href="<?php the_permalink(); ?>" class="btn btn-alter"><?php echo esc_html(get_theme_mod('the_multiple_contact_section_readmore')); ?></a>
                                 </div>
                            </div>       
                           <?php
                        endwhile;
                         wp_reset_postdata();
                    endif; 
                      ?>
                </div>
        </section>
      <?php
  endif;
     }
        ?>