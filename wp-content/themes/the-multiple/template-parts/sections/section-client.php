<?php
/**
 *Client Section 
*/
if(get_theme_mod('the_multiple_homepage_setting_our_client_section_option','no') == 'yes'):
   $client_news_cat = esc_attr(get_theme_mod('themultiple_homepage_setting_our_client_section_category',''));
  if($client_news_cat):
    $i=0;
    $args_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>-1, 'category_name' => $client_news_cat));  
    if ($args_query->have_posts()) :?>        
          <section id="client-section" class="the-multiple-client-section">
              <div class="carousel section-wrapper">
                    <div class="bt-container">
                        <div class="row">
                              <div class="carousel2-content tcenter clearfix">
                                   <div class="affiliation-slider">
                                        <?php
                                        while ($args_query->have_posts()):
                                            $args_query->the_post();
                                            $client_slider_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'the-multiple-client-thumb',true);
                                            $i++;
                                            ?>
                                            <div class="client-block<?php if($i%5==0){echo " nomargin";} echo ' client-post-'.$i;?> wow fadeInLeft" data-wow-delay="<?php echo $i*0.3;?>">
                                                <div class="client_the_multiple">
                                                   <div class="cat-caption">
                                                          <?php if(has_post_thumbnail()): ?>
                                                          <div class="client-news-image">
                                                           <img src="<?php echo esc_url($client_slider_image[0]); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" />
                                                          </div>
                                                          <?php endif; ?>
                                                     </div>
                                                  </div>
                                              </div>  

                                            <?php
                                        endwhile;
                                        wp_reset_postdata();
                                        ?>
                                    </div>
                              </div>
                        </div>
                    </div>
                </div>     
            </section>
        <?php 
      endif;
  endif;
endif;
  ?>