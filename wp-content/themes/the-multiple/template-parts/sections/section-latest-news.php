<?php
/**
 *Latest News Section 
*/
    if(get_theme_mod('themultiple_homepage_setting_latest_section_option','no') == 'yes'):
        $latest_news_cat = esc_attr(get_theme_mod('themultiple_homepage_setting_latest_news_section_category',''));
        $latest_news_title = get_theme_mod('the_multiple_homepage_setting_latest_news_section_menu_title_text',esc_html__('Latest News','the-multiple'));
         if($latest_news_cat):
            $i=0;
            $args_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>6, 'category_name' => $latest_news_cat));  
            if ($args_query->have_posts()) :?>    
                <section id="latest-news-section" class="the-multiple-latest-news-section">
                    <div class="bt-container">
                        <h2 class="section-title">            
                            <?php echo esc_html($latest_news_title);?>
                        </h2>
                        <?php
                        
                        while ($args_query->have_posts()):
                            $args_query->the_post();
                            $latest_news_slider_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'the-multiple-latest-news-thumb');
                             $i++;
                                    ?>
                            <div class="latest-single <?php if($i%4==0){echo " nomargin";} ?>  wow fadeInRight"  data-wow-delay="0.8s">
                                    <div class="cat-caption">
                                        <?php if(has_post_thumbnail()): ?>
                                        <div class="latest-news-image">
                                           <img src="<?php echo esc_url($latest_news_slider_image[0]); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" />
                                        </div>
                                        <?php endif; ?>
                                        <div class="latest-news-context"> 
                                            <h2 class="latest-news-caption-title">
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                            <div class="latest-news-desc">
                                                <?php echo esc_attr(wp_trim_words(get_the_content(),100,'&hellip;')); ?>
                                             </div>
                                            <div class="block-poston"><?php do_action( 'the_multiple_home_posted_on' );?></div>
                                        </div>   
                                </div>
                        </div> 

                            <?php
                        endwhile;
                                wp_reset_postdata();
                                ?>
                    </div>
                            
                 </section>
                    <?php 
             endif;
        endif;
    endif
        ?>