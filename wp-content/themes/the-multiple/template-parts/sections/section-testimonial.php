<?php
/**
 * Testimonial Section
 * @package the-multiple
 */
?>
<?php
    if(get_theme_mod('the_multiple_testimonial_section','no') == 'yes'):
        $testimonial_cat = esc_attr(get_theme_mod('the_multiple_testimonial_section_category',''));
        $testimonial_title = get_theme_mod('testimonial_homepage_title',esc_html__('What People Are Saying About US','the-multiple'));
        $testimonial_image = get_theme_mod('the_multiple_testimonial_bkgimage','');
        $the_multiple_testimonial_section_layout = get_theme_mod('the_multiple_testimonial_section_layout','layout-1');
         if($testimonial_cat):
            $i=0;
            $args_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>8, 'category_name' => $testimonial_cat));  
            if ($args_query->have_posts()) :?>    
                 <section id="testimonial-section" class="the-multiple-testimonial-section <?php echo esc_attr($the_multiple_testimonial_section_layout); ?>" data-stellar-background-ratio="0.5" style="background-image: url('<?php echo esc_url($testimonial_image); ?>');">
                    <div class="bt-container">
                            <h2 class="section-title wow slideInUp"><?php echo esc_html($testimonial_title);?></h2>
                           <?php if($the_multiple_testimonial_section_layout != 'layout-2' ){ ?>     
                           <div class="testimonial">
                                    <?php } ?> 
                                       <?php   
                                       while ($args_query->have_posts()):
                                        $args_query->the_post();
                                        $testimonial_slider_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'the-multiple-testimonial-thumb-image');
                                         $i++;
                                                ?>      
                                            <div class="testimonial-single <?php if($i%4==0){echo " nomargin";} ?>  wow fadeInUp" data-wow-delay="0.8s">
                                                    <div class="cat-caption">
                                                        <?php if(has_post_thumbnail()): ?>
                                                        <?php endif; ?>
                                                        <div class="testimonial-context"> 
                                                            <h2 class="testimonial-caption-title">
                                                                <?php the_title(); ?>
                                                            </h2>
                                                            <div class="testimonial-desc">
                                                                <?php echo esc_attr(wp_trim_words(get_the_content(),150,'&hellip;')); ?>
                                                             </div>
                                                        </div>
                                                        <div class="testimonial-image">
                                                           <img src="<?php echo esc_url($testimonial_slider_image[0]); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" />
                                                        </div>
                                                    </div>
                                             </div> 
                                                 
                                          <?php
                                         endwhile;
                                      wp_reset_postdata();
                                     ?>
                               </div>
                         </div>    
                 </section>
                 <?php 
             endif;
        endif;
    endif
        ?>