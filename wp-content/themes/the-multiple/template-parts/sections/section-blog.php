<?php
/**
 *Blog Section 
*/
    if(get_theme_mod('the_multiple_home_blog_setting','no') == 'yes'):
        $blog_cat = esc_attr(get_theme_mod('the_multiple_blog_section_category',''));
        $blog_section_title = get_theme_mod('the_multiple_home_blog_title',esc_html__('Our Blog','the-multiple'));
        $blog_readmore = esc_html(get_theme_mod('the_multiple_blog_section_readmore',esc_html__('Read More','the-multiple')));
        $blogimage = get_theme_mod('the_multiple_blog_bkgimage','');
        $the_multiple_blog_section_layout = get_theme_mod('the_multiple_blog_section_layout','layout-1');
         if($blog_cat):
            $i=0;
            $args_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>6, 'category_name' => $blog_cat));  
            if ($args_query->have_posts()) :?>    
                <section id="blog-section" class="the-multiple-blog-section <?php echo esc_attr($the_multiple_blog_section_layout); ?>" style="background-image: url('<?php echo esc_url($blogimage); ?>'); ">
                    <div class="bt-container">
                        <h2 class="section-title">            
                            <?php echo esc_html($blog_section_title);?>
                        </h2>
                        <div class="blog-section-wrap">
                        <?php
                        while ($args_query->have_posts()):
                            $args_query->the_post();
                            $blog_section_slider_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'the-multiple-blog-image');
                             $i++;
                                    ?>
                            <div class="blog-single <?php if($i%4==0){echo " nomargin";} ?>  wow fadeInRight"  data-wow-delay="0.8s">
                                <div class="cat-caption">
                                    <?php if(has_post_thumbnail()): ?>
                                        <div class="blog-section-image">
                                           <img src="<?php echo esc_url($blog_section_slider_image[0]); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" />
                                        </div>
                                        <?php endif; ?>
                                        <div class="blog-context"> 
                                            <div class="block-poston"><?php do_action( 'the_multiple_home_posted_on' );?></div>
                                            <h2 class="blog-caption-title">
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                            <div class="blog-desc">
                                                <?php echo esc_attr(wp_trim_words(get_the_content(),12,'&hellip;')); ?>
                                             </div>
                                             <?php 
                                             ?>
                                            <?php if($the_multiple_blog_section_layout != 'layout-2'){
                                                ?> 
                                                 <?php
                                                if($blog_readmore){
                                                    ?>
                                                    <a href="<?php the_permalink();?>" class="btn more"><?php echo $blog_readmore;?></a>
                                                    <?php
                                                    }
                                             } ?>
                                        </div>   
                                 </div>
                              </div> 

                            <?php
                        endwhile;
                                wp_reset_postdata();
                                ?>
                        </div>
                    </div>         
                 </section>
                    <?php 
             endif;
        endif;
    endif
        ?>