<?php
/**
 * Feature Section 
*/
//starting feature section
if(get_theme_mod('themultiple_homepage_setting_feature_section_option','no') == 'yes'):
      $feature_cat = esc_attr(get_theme_mod('themultiple_homepage_setting_feature_section_category',''));
      $feature_readmore = esc_html(get_theme_mod('themultiple_setting_feature_section_readmore',esc_html__('Read More','the-multiple')));
      $the_multiple_feature_section_layout = get_theme_mod('the_multiple_feature_section_layout','layout-1');
        if($feature_cat):
            $i=0;
            $args_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>4,'category_name' => $feature_cat));  
            if ($args_query->have_posts()) :?>
                  <section id="feature-section" class="the-multiple-feature-section <?php echo esc_attr($the_multiple_feature_section_layout); ?>">
                        <div class="bt-container">
                              <?php
                              while ($args_query->have_posts()):
                                    $args_query->the_post();
                                    $feature_slider_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'the_multiple-feature-thumb');
                                    $i++;
                                    ?>
                                      <div class="feature_the_multiple">
                                          <div class="featured-block<?php if($i%5==0){echo " nomargin";} echo ' featured-post-'.$i;?> wow fadeInLeft"  data-wow-delay="<?php echo $i*0.3;?>s">
                                             <div class="cat-caption">
                                                    <?php if(has_post_thumbnail()): ?>
                                                    <div class="feature-image">
                                                     <img src="<?php echo esc_url($feature_slider_image[0]); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" />
                                                    </div>
                                                    <?php endif; ?>
                                                   <div class="the-multiple-feature-caption"> 
                                                        <h2 class="feature-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> 
                                                          <div class="the-multiple-feature-desc">
                                                           <?php echo esc_attr(wp_trim_words(get_the_content(),12,'&hellip;')); ?>
                                                           </div>
                                                        <?php if($the_multiple_feature_section_layout != 'layout-2'){
                                                          ?> 
                                                          <?php
                                                          if($feature_readmore){
                                                              ?>
                                                              <a href="<?php the_permalink();?>" class="btn more"><?php echo $feature_readmore;?></a>
                                                             <?php
                                                           }
                                                        } ?>
                                                        <?php 
                                                        
                                                         ?> 
                                                        
                                                  </div>
                                               </div>
                                           </div>
                                      </div>
                                       
                                    <?php
                              endwhile;
                              wp_reset_postdata();
                              ?>
                      </div>
                  </section>
                  <?php 
            endif;
        endif;
  endif;
  ?>
