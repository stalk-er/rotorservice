<?php
/** FAQ Section **/

    
        if(get_theme_mod('the_multiple_faq_section_option','no') == 'yes'):
        $faq_section_title = get_theme_mod( 'the_multiple_faq_title');
        $faq_section_description = get_theme_mod( 'the_multiple_faq_desc');
        $the_multiple_faq_cat_id = get_theme_mod('the_multiple_faq_section_category');
        $the_multiple_faq_cat_id = get_category_by_slug( $the_multiple_faq_cat_id )->term_id;
        $faq_list_title = get_theme_mod('the_multiple_faq_second_title');
        $faqimage = get_theme_mod('the_multiple_faq_bkgimage','');
?>
        <section id="faq-section" class="the-multiple-section-faq"  style="background-image: url('<?php echo esc_url($faqimage); ?>');">
            <div class="bt-container">
                   <?php if($faq_section_title || $faq_section_description){ ?>
                    <div class="left-side wow slideInUp" data-wow-delay="0.4s" >
                            <?php if($faq_section_title){ ?><h2><?php echo esc_html($faq_section_title); ?></h2><?php } ?>
                            <?php if($faq_section_description){ ?>
                            <p class="faq-des wow slideInUp" data-wow-delay="0.6s" >
                                <?php echo esc_html($faq_section_description); ?> 
                            </p>
                            <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if($the_multiple_faq_cat_id){
                     $i=0;   
                    $the_multiple_faq_query = new WP_Query(array('post_type'=>'post','cat'=>$the_multiple_faq_cat_id,'posts_per_paage'=>5));
                    if($the_multiple_faq_query->have_posts()){ 
                        $i++; ?>
                        <div class="right-side <?php if($i%4==0){echo " nomargin";} ?>  wow slideInUp " data-wow-delay="0.4s">
                              <?php if($faq_list_title){ ?><h2><?php echo esc_attr($faq_list_title); ?></h2><?php } ?>
                                    <?php while($the_multiple_faq_query->have_posts()){
                                        $the_multiple_faq_query->the_post();
                                        if(get_the_content() || get_the_title()){ ?>
                                            <ul class="accordion">
                                                <li>
                                                    <a href=""><?php the_title(); ?></a>
                                                    <p><?php echo esc_attr(wp_trim_words(get_the_content(),150,'&hellip;')); ?></p>
                                                </li>
                                            </ul>
                                    <?php }
                                } ?>
                        </div>
              </div>
                    <?php }
                   } ?>
         </section>
    <?php
endif;
?>