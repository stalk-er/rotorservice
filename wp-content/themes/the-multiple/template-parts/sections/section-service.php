<?php
/**
 *Service Section 
*/
    if(get_theme_mod('themultiple_homepage_setting_service_section_option','no')=='yes'){ 
        $service_post1 = get_theme_mod('the_multiple_service_post1');
        $service_post2 = get_theme_mod('the_multiple_service_post2');
        $service_post3 = get_theme_mod('the_multiple_service_post3');
        $service_post1_icon = get_theme_mod('one_service_icon','fa-trophy');
    	  $service_post2_icon = get_theme_mod('second_service_icon','fa-print');
        $service_post3_icon = get_theme_mod('third_service_icon','fa-group');
        $service_post_readmore = get_theme_mod('themultiple_setting_service_section_readmore', esc_html__('Know More','the-multiple'));
        $serviceimage = get_theme_mod('the_multiple_service_bkgimage','');
		?>      
		<section id="service-section" class="the-multiple-service-post-section" style="background-image: url('<?php echo esc_url($serviceimage); ?>');background-size:cover;background-positon:center;">
			    <div class="bt-container"> 
          		     <div class="post-service-wrapper">
          			        <?php if(!empty($service_post1) || !empty($service_post2) || !empty($service_post3)){
          			        if(!empty($service_post1)) { ?>
          			            <div id="service-post-1" class="service-post wow slideInUp" data-wow-delay="0.4s" >
          			                 <?php
          			                 $query1 = new WP_Query( 'p='.$service_post1 );
          			                  // the Loop
          			                 while ($query1->have_posts()) : $query1->the_post();                                                        
                			                  ?>
                			            <div class="service-icon">
                			                  <i class="fa <?php echo esc_attr($service_post1_icon); ?>"></i>
                			             </div>
                			            <div class="service-content">
                			                <h2 class="service-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                			                <div class="service-slide-desc">
                                              <?php echo esc_attr(wp_trim_words(get_the_content(),500,'&hellip;')); ?>
                                      </div>
                			            </div>
          			                
          			                  <?php endwhile;
          			                  wp_reset_postdata(); ?>             
          			            </div>
          			        <?php } 

          			         if(!empty($service_post2)) { ?>
          			            <div id="service-post-2" class="service-post wow slideInUp" data-wow-delay="0.6s" >
          			                   <?php
          			                  $query2 = new WP_Query( 'p='.$service_post2 );
          			                        // the Loop
          			                 while ($query2->have_posts()) : $query2->the_post();   ?>
          			                    <div class="service-icon">
          			                        <i class="fa <?php echo esc_attr($service_post2_icon); ?>"></i>
          			                    </div>                           
          			                    <div class="service-content">
          			                        <h2 class="service-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>  
          			                        <div class="service-slide-desc">
                                                <?php echo esc_attr(wp_trim_words(get_the_content(),500,'&hellip;')); ?>
                                        </div>                               
          			                    </div>
          			                 <?php endwhile;
          			                     wp_reset_postdata(); ?>             
          			            </div>
          			                <?php } 
          			            if(!empty($service_post3)) { ?>
          			                <div id="service-post-3" class="service-post wow slideInUp" data-wow-delay="0.8s">
          				                  <?php
          				                  $query3 = new WP_Query( 'p='.$service_post3 );
          				                        // the Loop
          				                while ($query3->have_posts()) : $query3->the_post();   ?>
          				                    <div class="service-icon">
          				                        <i class="fa <?php echo esc_attr($service_post3_icon); ?>"></i>
          				                    </div>  
          				                    <div class="service-content">
          				                        <h2 class="service-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> 
          				                        <div class="service-slide-desc">
                                                 <?php echo esc_attr(wp_trim_words(get_the_content(),500,'&hellip;')); ?>
                                          </div>                               
          				                    </div>
          				            
          				                 <?php endwhile;
          				                     wp_reset_postdata(); ?>             
          			                 </div>
                                <?php }
                              ?>
          			       </div>
          			            <div class="wrap-law-post-right " >
                                <div class="text-service wow fadeInUp" data-wow-delay="1s">
            						            <h1><?php echo esc_html(get_theme_mod('the_multiple_homepage_setting_service_title_text',esc_html__('Our Advantages','the-multiple')));?></h1>
                                </div>
                                <div class="text-service-des wow fadeInUp" data-wow-delay="1.2s">
          						                <p><?php echo wp_kses_post(get_theme_mod('the_multiple_service_desc'));?></p>
                                </div>
                                <div <div class="text-service-read-more wow fadeInUp" data-wow-delay="1.7s">
          						                <?php      
                                   if(!empty($service_post_readmore)){?>
          						             <a href="<?php echo esc_url($law_post_btn_link);?>" class="view-more"><?php echo esc_html($service_post_readmore); ?></a>
          						              <?php } ?>
                                  </div>
                              </div>
          			          <?php } ?>   
          			    <?php } ?>
			      </div>
		 </section>            
        <?php  ?>
