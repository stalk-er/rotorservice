<?php
/**
 * About Section 
*/
//starting about us section
  if (get_theme_mod('themultiple_homepage_setting_about_section_option','no')=='yes') {
      $post_id = get_theme_mod('the_multiple_about_post');  
      $the_multiple_about_section_layout = get_theme_mod('the_multiple_about_section_layout','layout-1');
      if(!empty($post_id)):
              ?>
          <section id="about-section" class="the-multiple-about-section <?php echo esc_attr($the_multiple_about_section_layout); ?>">
              <div class="bt-container">
                  <?php 
                  $about_args  = array('post_type'=>'post', 'p' => $post_id, 'post_status' => 'publish','posts_per_page'=>1);
                  $about_query = new WP_Query($about_args);
                    if ($about_query->have_posts()):
                        while ($about_query->have_posts()):
                            $about_query->the_post();
                            ?>
                            <div class="about-content-wrap">
                                 <div id="about-post" class="about-post wow fadeInUp" data-wow-delay="0.4s" >
                                    <h2 class="section-title"><?php the_title(); ?></h2>
                                 </div>
                                 <div class="slide-desc">
                                    <div id="about-post-1" class="about-post wow fadeInUp" data-wow-delay="0.6s" >
                                    <?php echo esc_attr(wp_trim_words(get_the_content(),100,'&hellip;')); ?>
                                    </div>
                                 </div>
                                 <div id="about-post-1" class="about-post wow fadeInUp" data-wow-delay="0.8s" >
                                   <div class="btn-wrapper"><a href="<?php the_permalink(); ?>" class="btn more"><?php echo esc_html(get_theme_mod('themultiple_setting_about_section_readmore',esc_html__('Know More','the-multiple'))); ?></a>
                                   </div>
                                 </div>
                            </div>
                            <?php
                              $aboutimage = get_theme_mod('the_multiple_about_bkgimage','the-multiple-about-image');
                              if(!empty($aboutimage)){ ?>      
                          
                           <?php }
                           ?>         
                           <?php
                        endwhile;
                         wp_reset_postdata();
                    endif; 
                      ?>
                      <?php if($the_multiple_about_section_layout != 'layout-2'){
                            ?> 
                            <div class="about-img wow fadeInRight" data-wow-delay="1s">
                             <a href="<?php the_permalink();?>"><img src="<?php echo esc_url($aboutimage); ?>" /></a>
                           </div><?php
                        } ?>
                </div>
          </section>
      <?php
    endif;
}
?>