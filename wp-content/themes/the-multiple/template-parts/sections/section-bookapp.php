<?php
/**
 *Book And Appoinment Section 
*/
if(get_theme_mod('themultiple_homepage_setting_book_and_appoinment_section_option')=='yes'){
	$book_and_app = esc_html(get_theme_mod('the_multiple_book_and_appointment_small_title',esc_html__('Need a Doctor for check Up','the-multiple')));
		if(!empty($book_and_app)):
							?>
		    <section id="book-app-section" class="the-multiple-book-and-app">
			    <div class="bt-container">
					<?php
                     $bookappimage = esc_attr(get_theme_mod('the_multiple_book_and_appointment_bkgimage'));
                     if(!empty($bookappimage)){ ?>       
                      <div class="book-and-app-image">
                          <a href="<?php the_permalink();?>"><img src="<?php echo esc_url($bookappimage); ?>"/>
                          </a>
                      </div>
                       <?php  }
					            ?>						
					    <div class="content-wrap">
									    <?php  ?>
							<h2 class="title cta-small-title home-title wow fadeInDown"><?php echo $book_and_app; ?></h2>
		                    <div class="cta-link-small wow fadeInRight" data-wow-delay="0.5s"><a href="<?php echo esc_url(get_theme_mod('the_multiple_book_app_link_small','#')); ?>" class="btn more"><?php echo esc_attr(get_theme_mod('the_multiple_book_and_appointment_readmore',esc_html__('GET AN APPOINTMENT','the-multiple'))); ?></a>
		                    </div>
					    </div>
		        </div>
			</section>
		     <?php
    endif;
 }
 ?>