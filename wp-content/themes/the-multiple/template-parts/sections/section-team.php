<?php
/**
 *Team Section 
*/
    if(get_theme_mod('the_multiple_homepage_setting_team_section_option','no') == 'yes'):
         $team_cat = esc_attr(get_theme_mod('themultiple_homepage_setting_team_section_category',''));
         $team_title = get_theme_mod('the_multiple_homepage_setting_team_title_text',esc_html__('Our Team','the-multiple'));
         $the_multiple_team_section_layout = get_theme_mod('the_multiple_team_section_layout','layout-1');
         if($team_cat):
            $i=0;
             $args_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' =>6, 'category_name' => $team_cat));  
            if ($args_query->have_posts()) :?> 
                <section id="team-section" class="the-multiple-team-section <?php echo esc_attr($the_multiple_team_section_layout); ?>">
                    <div class="bt-container">
                        <h2 class="section-title">            
                            <?php echo esc_html($team_title);?>
                        </h2>
                        <?php if($the_multiple_team_section_layout != 'layout-2' ){ ?>
                        <div class="team-slider">
                           <?php } ?>
                          <?php
                         while ($args_query->have_posts()):
                            $args_query->the_post();
                            $team_slider_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'the-multiple-team-thumb');
                            $i++;
                             ?>
                              <div class="team-slide">
                                    <div class="team-section <?php if($i%4==0){echo " nomargin";} ?>  wow fadeInRight"  data-wow-delay="0.8s">
                                       <div class="team_the_multiple">
                                           <div class="cat-caption">
                                                <?php if(has_post_thumbnail()): ?>
                                               <div class="team-image">
                                                  <img src="<?php echo esc_url($team_slider_image[0]); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" />
                                                </div>
                                                <?php endif; ?>
                                                 <div class="team-caption"> 
                                                     <h2 class="team-caption-title">
                                                     <?php the_title();?></h2> 
                                                     <div class="team-slide-desc">
                                                       <?php echo esc_attr(wp_trim_words(get_the_content(),12,'&hellip;')); ?>
                                                     </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                         <?php
                        endwhile;
                       wp_reset_postdata();
                        ?>
                       </div>
                    </div>  
                            
                </section>
                 <?php 
            endif;
        endif;
    endif;
        ?>