<?php
/**
 * the-multiple Theme Customizer Custom
 *
 * @package the-multiple
 */
/**
 * Add new options the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

function the_multiple_custom_customize_register( $wp_customize ) {
	// the-multiple posts list.
    $the_multiple_category_lists 	=	the_multiple_category_lists();
    $posts_list = the_multiple_post_list();
    // the-multiple sanitizer
    require get_template_directory() . '/inc/the-multiple-sanitizer.php';
    /** Upgrade to Bloggerbuz **/
	$wp_customize->register_section_type( 'the_multiple_Link_Section' );

	// Register sections.
	$wp_customize->add_section(
	    new the_multiple_Link_Section(
	        $wp_customize,
	        'the-multiple-pro',
	        array(
	            'title'    => esc_html__( 'Upgrade To The Multiple Pro', 'the-multiple' ),
	            'pro_text' => esc_html__( 'Go Pro','the-multiple' ),
	            'pro_url'  => 'https://buzthemes.com/wordpress_themes/the-multiple-pro/',
	            'priority' => 1,
	        )
	    )
	);
    /*/** Theme Info section **/
	$wp_customize->add_section(
	    'the_multiple_theme_info_section',
	    array(
	        'title'		=> esc_html__( 'Theme Info', 'the-multiple' ),
	        'priority'  => 1,
	    )
	);
	// More Themes
	$wp_customize->add_setting(
	    'the_multiple_por_information', 
	    array(
	        'type'              => 'theme_info',
	        'capability'        => 'edit_theme_options',
	        'sanitize_callback' => 'esc_attr',
	    )
	);
	$wp_customize->add_control( new the_multiple_Theme_Info( 
	    $wp_customize ,
	    'the_multiple_por_information',
	        array(
	          'label' => esc_html__( 'The Multiple Pro Theme' , 'the-multiple' ),
	          'section' => 'the_multiple_theme_info_section',
	        )
	    )
	);

    

	//Adding the Default Setup Panel
	$wp_customize->add_panel('the_multiple_default_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Default Setups','the-multiple'),
		));	

	//Add Default Sections to General Panel
	$wp_customize->get_section('title_tagline')->panel = 'the_multiple_default_setups'; //priority 20
	$wp_customize->get_section('colors')->panel = 'the_multiple_default_setups'; //priority 40
	$wp_customize->get_section('header_image')->panel = 'the_multiple_default_setups'; //priority 60
	$wp_customize->get_section('background_image')->panel = 'the_multiple_default_setups'; //priority 80
	$wp_customize->get_section('static_front_page')->panel = 'the_multiple_default_setups'; //priority 120

    //color setting
     $wp_customize->add_panel('themultiple_theme_color',array(
		'priority' => '20',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Theme Color','the-multiple'),		
		));
    $wp_customize->add_section( 'the_multiple_colors', array(
	     'title' => esc_html__( 'Skin Color' , 'the-multiple'),
	     'panel' => 'themultiple_theme_color',
	) );

	$wp_customize->add_setting('the_multiple_skim_color',
                    array(
		                'default'           =>  '#2196F3',
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );
    $wp_customize->add_control('the_multiple_skim_color',array(
		                'description'   =>  esc_html__('Choose the theme Skim Color','the-multiple'),
		                'section'       =>  'the_multiple_colors',
		                'setting'       =>  'the_multiple_skim_color',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                   '#2196F3'=>esc_html__('Default', 'the-multiple'),
                           '#ffa000' =>esc_html__('Amber', 'the-multiple'),
                            '#ff7043'=>esc_html__('Orange','the-multiple'),
		                    )
		                )                   
		            );
     
    //adding general setting panel
     $wp_customize->add_panel('themultiple_general_setups',array(
		'priority' => '30',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('General Setup','the-multiple'),		
		));

	/* Header Section */
    //Adding the General Setup section
    $wp_customize->add_section('the_multiple_header_menu_layout',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Header Menu Layout ','the-multiple'),
		'description' => esc_html__('Manage general setting for the site','the-multiple'),
		'panel' => 'themultiple_general_setups'
		));
    $wp_customize->add_setting(
	        'the_multiple_header_layout',
	        array(
	            'default'           => 'layout-1',
	            'sanitize_callback' => 'sanitize_text_field',
	        )
	    );	    
	$wp_customize->add_control(
	        'the_multiple_header_layout',
	            array(
	                'label'    => esc_html__( 'Header Menu Layout', 'the-multiple' ),
	                'section'  => 'the_multiple_header_menu_layout',
                    'type' => 'radio',
	                'choices'  => array(
		                    'layout-1' => esc_html__( 'Layout 1', 'the-multiple' ),
		                    'layout-2' => esc_html__( 'Layout 2', 'the-multiple' ),
                            'layout-3' => esc_html__( 'Layout 3', 'the-multiple' ),
		            ),
		            'priority' => 6
	            )
	    );
	$wp_customize->add_section('themultiple_top_header',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Show Header Search ','the-multiple'),
		'description' => esc_html__('Manage general setting for the site','the-multiple'),
		'panel' => 'themultiple_general_setups'
		));
	//Enable/Disable for General setup
	$wp_customize->add_setting('the_multiple_general_search_top_header',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_general_search_top_header',array(
		                'description'   =>  esc_html__('Do you want to enable search on header','the-multiple'),
		                'section'       =>  'themultiple_top_header',
		                'setting'       =>  'the_multiple_general_search_top_header',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    //starting call to
    $wp_customize->add_section(
    	'the_multiple_header_setting_callto',
    	array(
			'title' => esc_html__('Call-To','the-multiple'),
			'priority' => '10',
			'panel' => 'themultiple_general_setups'
			)
    	);
    //call to one
	$wp_customize->add_setting(
			'the_multiple_header_setting_callto_text_one',
			array(
				'default' => '',
				'sanitize_callback' => 'the_multiple_sanitize_text',
				)
			);
	$wp_customize->add_control(
			'the_multiple_header_setting_callto_text_one',
			array(
				'type' => 'textarea',
				'label' => esc_html__('Call To Content One','the-multiple'),
				'description' => esc_html__('Enter text or HTML for call to actions','the-multiple'),
				'section' => 'the_multiple_header_setting_callto'
				)
			);
	// call to second
	$wp_customize->add_setting(
			'the_multiple_header_setting_callto_text_two',
			array(
				'default' => '',
				'sanitize_callback' => 'the_multiple_sanitize_text',
				)
			);
	$wp_customize->add_control(
			'the_multiple_header_setting_callto_text_two',
			array(
				'type' => 'textarea',
				'label' => esc_html__('Call To Content Two','the-multiple'),
				'description' => esc_html__('Enter text or HTML for call to action','the-multiple'),
				'section' => 'the_multiple_header_setting_callto'
				)
			);
	//starting logo alignment 
    $wp_customize->add_section('the_multiple_logo_alignment', array(
       	'priority' => 50,
       	'title' => esc_html__('Logo Alignment', 'the-multiple'),
       	'panel' => 'themultiple_general_setups'
	));
    $wp_customize->add_setting('the_multiple_logo_alignment', array(
      'default' => 'Left',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'the_multiple_radio_sanitize_alignment_logo',
    ));

    $wp_customize->add_control('the_multiple_logo_alignment', array(
      'type' => 'radio',
      'label' => esc_html__('Choose the layout that you want','the-multiple'),
      'section' => 'the_multiple_logo_alignment',
      'setting' => 'the_multiple_logo_alignment',
      'choices' => array(
         'left'=>esc_html__('Left', 'the-multiple'),
         'center'=>esc_html__('Center','the-multiple'),
      )
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_logo_upload', array(
		'label' => esc_html__('Upload logo for your site', 'the-multiple'),
		'section' => 'the_multiple_logo_alignment',
		'setting' => 'the_multiple_logo_upload'
	)));
     //starting weblyout setting
     $wp_customize->add_section('the_multiple_weblayout', array(
       	'priority' => 50,
       	'title' => esc_html__('Web Layout', 'the-multiple'),
       	'panel' => 'themultiple_general_setups'
	)); 
     $wp_customize->add_setting('the_multiple_weblayout', array(
      'default' => 'fullwidth',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'the_multiple_radio_sanitize_webpagelayout',
    ));

     $wp_customize->add_control('the_multiple_weblayout', array(
      'type' => 'radio',
      'label' => esc_html__('Choose the layout that you want', 'the-multiple'),
      'section' => 'the_multiple_weblayout',
      'setting' => 'the_multiple_weblayout',
      'choices' => array(
         'fullwidth' => esc_html__('Full  Layout', 'the-multiple'),
         'boxed' => esc_html__('Boxed Layout', 'the-multiple')
      )
   ));

   //Bredcrumbs Setting for the-multiple
  $wp_customize->add_section('the_multiple_bredcrumb',array(
    'title' => esc_html__('Page Breadcrumb','the-multiple'),
    'priority' => '40',
    'panel' => 'themultiple_general_setups'
    ));

  $wp_customize->add_setting(
    'the_multiple_page_bg_image',
    array(
        'default' => '',
        'sanitize_callback' => 'esc_url_raw'
    )
   );
  $wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'the_multiple_page_bg_image',
           array(
               'label'      => __( 'Page Breadcrumb Backgeound Image', 'the-multiple' ),
               'section'    => 'the_multiple_bredcrumb',
               'settings'   => 'the_multiple_page_bg_image',
               'priority' => 10,
           )
       )
   );  
   //Adding the Homepage Setup Panel
	$wp_customize->add_panel('themultiple_homepage_setups',array(
		'priority' => '30',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Homepage Setups','the-multiple'),		
		));
	
	//Adding the Slider Setup Panel
	$wp_customize->add_section('themultiple_slider_setups',array(
		'priority' => '01',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Slider Setups','the-multiple'),
		'description' => esc_html__('Manage Slides for the site','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
     //slider section
	$wp_customize->add_setting('themultiple_homepage_setting_slider_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_slider_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'themultiple_slider_setups',
		                'setting'       =>  'themultiple_homepage_setting_slider_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );

   	//select category for slider
   	$wp_customize->add_setting('themultiple_homepage_setting_slider_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control(
		            'themultiple_homepage_setting_slider_section_category',array(
		                'priority'      =>  20,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'themultiple_slider_setups',
		                'setting'       =>  'themultiple_homepage_setting_slider_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            );
    //slider read more text
    $wp_customize->add_setting(
		            'themultiple_setting_slider_section_readmore',array(
		                'default'           =>  esc_html__('Know more','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'themultiple_setting_slider_section_readmore',array(
		                'priority'      =>  25,
		                'label'         =>  esc_html__('Read more text','the-multiple'),
		                'section'       =>  'themultiple_slider_setups',
		                'setting'       =>  'themultiple_setting_slider_section_readmore',
		                'type'          =>  'text',  
		                )                                     
		            );
    //slider caption
    $wp_customize->add_setting('the_multiple_home_slider_caption',array(
	'default' => 'caption-left',
	'sanitize_callback' => 'sanitize_text_field'
	)
    );

   $wp_customize->add_control('the_multiple_home_slider_caption',array(
	'type' => 'radio',
	'label' => __('Slider Caption', 'the-multiple'),
	'description' => __('Choose how to show titles and description over Slider', 'the-multiple'),
	'section' => 'themultiple_slider_setups',
	'choices' => array(
		'caption-no' => __('No caption', 'the-multiple'),
		'caption-left' => __('Left Aligned', 'the-multiple'),
		'caption-center' => __('Center Aligned', 'the-multiple'),
		),
	)
    );
   //Adding the feature section
	$wp_customize->add_section('themultiple_feature_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Feature Section','the-multiple'),
		'description' => esc_html__('Manage feature section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
     
	$wp_customize->add_setting('themultiple_homepage_setting_feature_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_feature_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'themultiple_feature_setups',
		                'setting'       =>  'themultiple_homepage_setting_feature_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    $wp_customize->add_setting(
	        'the_multiple_feature_section_layout',
	        array(
	            'default'           => 'layout-1',
	            'sanitize_callback' => 'sanitize_text_field',
	        )
	    );	    
	$wp_customize->add_control(
	        'the_multiple_feature_section_layout',
	            array(
	                'label'    => esc_html__( 'Feature Section Layout', 'the-multiple' ),
	                'section'  => 'themultiple_feature_setups',
                    'type' => 'radio',
	                'choices'  => array(
		                    'layout-1' => esc_html__( 'Layout 1', 'the-multiple' ),
		                    'layout-2' => esc_html__( 'Layout 2', 'the-multiple' ),
                            'layout-3' => esc_html__( 'Layout 3', 'the-multiple' ),
		            ),
		            'priority' => 6
	            )
	    );
   	//select category for feature section
   	$wp_customize->add_setting('themultiple_homepage_setting_feature_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_feature_section_category',array(
		                'priority'      =>  20,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'themultiple_feature_setups',
		                'setting'       =>  'themultiple_homepage_setting_feature_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            );
    $wp_customize->add_setting(
		            'themultiple_setting_feature_section_readmore',array(
		                'default'           =>  esc_html__('Know more','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'themultiple_setting_feature_section_readmore',array(
		                'priority'      =>  25,
		                'label'         =>  esc_html__('Read more text','the-multiple'),
		                'section'       =>  'themultiple_feature_setups',
		                'setting'       =>  'themultiple_setting_feature_section_readmore',
		                'type'          =>  'text',  
		                )                                     
		            );
   //Starting About Us Section
    $wp_customize->add_section('the_multiple_about_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('About Us Section','the-multiple'),
		'description' => esc_html__('Manage About Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));

    $wp_customize->add_setting('themultiple_homepage_setting_about_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_about_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_about_setups',
		                'setting'       =>  'themultiple_homepage_setting_about_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    $wp_customize->add_setting(
	        'the_multiple_about_section_layout',
	        array(
	            'default'           => 'layout-1',
	            'sanitize_callback' => 'sanitize_text_field',
	        )
	    );	    
	$wp_customize->add_control(
	   'the_multiple_about_section_layout',
	            array(
	                'label'    => esc_html__( 'About Section Layout', 'the-multiple' ),
	                'section'  => 'the_multiple_about_setups',
                    'type' => 'radio',
	                'choices'  => array(
		                    'layout-1' => esc_html__( 'Layout 1', 'the-multiple' ),
		                    'layout-2' => esc_html__( 'Layout 2', 'the-multiple' ),
		            ),
		            'priority' => 6
	            )
	    );
	//select post for about section 
    $wp_customize->add_setting('the_multiple_about_post',array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'the_multiple_sanitize_number',
        'transport' => 'postMessage'
    ));
   
    $wp_customize->add_control('the_multiple_about_post', array(
        'type' => 'select',
        'label' => esc_html__('About Us Post','the-multiple'),
        'section' => 'the_multiple_about_setups',
        'setting' => 'the_multiple_about_post',
        'choices' => $posts_list
    ));
    // Read More Text For the About Us Section
    $wp_customize->add_setting(
		            'themultiple_setting_about_section_readmore',array(
		                'default'           =>  esc_html__('Know more','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'themultiple_setting_about_section_readmore',array(
		                'priority'      =>  25,
		                'label'         =>  esc_html__('Know more text','the-multiple'),
		                'section'       =>  'the_multiple_about_setups',
		                'setting'       =>  'themultiple_setting_about_section_readmore',
		                'type'          =>  'text',  
		                )                                     
		            );
    //About Us image
    $wp_customize->add_setting('the_multiple_about_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_about_bkgimage', array(
		'label' => esc_html__('Image for About Us', 'the-multiple'),
		'section' => 'the_multiple_about_setups',
		'setting' => 'the_multiple_about_bkgimage'
		)));
    //Starting Book And Appoinment Section
    $wp_customize->add_section('themultiple_book_and_appointment_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Book And Appoinment Section','the-multiple'),
		'description' => esc_html__('Just make an appointment & you are done','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));

    //Enable/Disable Book and Appointment
	$wp_customize->add_setting('themultiple_homepage_setting_book_and_appoinment_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_book_and_appoinment_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'themultiple_book_and_appointment_setups',
		                'setting'       =>  'themultiple_homepage_setting_book_and_appoinment_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    //Book And Appointment Title
	$wp_customize->add_setting('the_multiple_book_and_appointment_small_title', array(
		'default' => esc_html__('Need a Doctor for check Up','the-multiple'),
		'sanitize_callback' => 'sanitize_text_field',
		));

	$wp_customize->add_control('the_multiple_book_and_appointment_small_title',array(
		'type' => 'text',
		'label' => esc_html__('Book And Appointment Section Title','the-multiple'),
		'section' => 'themultiple_book_and_appointment_setups',
		'setting' => 'the_multiple_book_and_appointment_small_title'
		));
    //Book Appointment link
	$wp_customize->add_setting('the_multiple_book_app_link_small', array(
		'default' => '',
		'sanitize_callback' => 'sanitize_text_field',
		
		));
	$wp_customize->add_control('the_multiple_book_app_link_small',array(
		'type' => 'text',
		'label' => esc_html__('Book And Appointment Link','the-multiple'),
		'section' => 'themultiple_book_and_appointment_setups',
		'setting' => 'the_multiple_book_app_link_small'
		));
    //Book and Appointment read more
	$wp_customize->add_setting('the_multiple_book_and_appointment_readmore', array(
		'default' => esc_html__('GET AN APPOINTMENT','the-multiple'),
		'sanitize_callback' => 'sanitize_text_field',
		'transport' => 'postMessage'
		));

	$wp_customize->add_control('the_multiple_book_and_appointment_readmore',array(
		'type' => 'text',
		'label' => esc_html__('Book and Appointment Read More Text','the-multiple'),
		'section' => 'themultiple_book_and_appointment_setups',
		'setting' => 'the_multiple_book_and_appointment_readmore'
		));
    //Book And Appoinment Section background image
	$wp_customize->add_setting('the_multiple_book_and_appointment_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_book_and_appointment_bkgimage', array(
		'label' => esc_html__('Image for Book and Appointment', 'the-multiple'),
		'section' => 'themultiple_book_and_appointment_setups',
		'setting' => 'the_multiple_book_and_appointment_bkgimage'
		)));
    //Service Section

    $wp_customize->add_section('the_multiple_service_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Service Section','the-multiple'),
		'description' => esc_html__('Manage Service Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
    
     //Enable/Disable Service Section
	$wp_customize->add_setting('themultiple_homepage_setting_service_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_service_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_service_setups',
		                'setting'       =>  'themultiple_homepage_setting_service_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );

    // Service separator one

    $wp_customize->add_setting(
    	    'service_sec_separator',
    		array(
    		    'default' => '',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'service_sec_separator', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( ' Right  Service Section ', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_service_setups',
    	         'priority'  => '7'
    	            )	            	
    	        )
    	    );

    //Service Section Title
    $wp_customize->add_setting(
	                'the_multiple_homepage_setting_service_title_text',array(
	                    'default'           =>  esc_html__('Our Advantages','the-multiple'),
	                    'sanitize_callback' =>  'sanitize_text_field',
	                    'transport'			=>	'postMessage',
	                    )
	                );

	            $wp_customize->add_control(
	                'the_multiple_homepage_setting_service_title_text',array(
	                    'priority'      =>  9,
	                    'label'         =>  esc_html__('Service title text','the-multiple'),
	                    'section'       =>  'the_multiple_service_setups',
	                    'setting'       =>  'the_multiple_homepage_setting_service_title_text',
	                    'type'          =>  'text',  
	                    )                                     
	                );

    //Service section description
    $wp_customize->add_setting('the_multiple_service_desc', array(
    'default' => '',
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));
    
    $wp_customize->add_control('the_multiple_service_desc',array(
        'type' => 'textarea',
        'label' => esc_html__('Service Section Description','the-multiple'),
        'section' => 'the_multiple_service_setups',
        'setting' => 'the_multiple_service_desc'
    ));

    //know more text for service section
    $wp_customize->add_setting(
		            'themultiple_setting_service_section_readmore',array(
		                'default'           =>  esc_html__('Know more','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'themultiple_setting_service_section_readmore',array(
		                'priority'      =>  10,
		                'label'         =>  esc_html__('Read more text','the-multiple'),
		                'section'       =>  'the_multiple_service_setups',
		                'setting'       =>  'themultiple_setting_service_section_readmore',
		                'type'          =>  'text',  
		                )                                     
		            );
    // Service separator two
    $wp_customize->add_setting(
    	    'service_sec_separator_two',
    		array(
    		    'default' => '',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'service_sec_separator_two', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( ' Left  Service Section', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_service_setups',
    	         'priority'  => '10'
    	            )	            	
    	        )
    	    );

	 //select post for Service Section 
    $wp_customize->add_setting('the_multiple_service_post1',array(
        'default' => '',
        'sanitize_callback' => 'the_multiple_sanitize_number',
        'capability' => 'edit_theme_options',
    ));
   
    $wp_customize->add_control('the_multiple_service_post1', array(
        'type' => 'select',
        'label' => esc_html__('Service Post 1','the-multiple'),
        'section' => 'the_multiple_service_setups',
        'setting' => 'the_multiple_service_post1',
        'choices' => $posts_list
    ));

    //first service icon
    $wp_customize->add_setting(
    	     'one_service_icon',
    		    array(
    		    'default' => '',
    		    'sanitize_callback' => 'sanitize_text_field',
    		     'transport' => 'postMessage'
    		     )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Icons_Control(
    	    $wp_customize, 
    	        'one_service_icon', 
    	         array(
    	        'type' 		=> 'the_multiple_icons',	                
    	        'label' 	=> esc_html__( 'First Service Icon', 'the-multiple' ),
    	        'description' 	=> esc_html__( 'Choose the icon from lists.', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_service_setups',
    	            )	            	
    	        )
    	    );
    //select post for service section
    $wp_customize->add_setting('the_multiple_service_post2',array(
        'default' => '',
        'sanitize_callback' => 'the_multiple_sanitize_number',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage'
    ));
   
    $wp_customize->add_control('the_multiple_service_post2', array(
        'type' => 'select',
        'label' => esc_html__('Service Post 2','the-multiple'),
        'section' => 'the_multiple_service_setups',
        'setting' => 'the_multiple_service_post2',
        'choices' => $posts_list
    ));
    // service icons two
    $wp_customize->add_setting(
    	     'second_service_icon',
    		    array(
    		    'default' => '',
    		    'sanitize_callback' => 'sanitize_text_field',
    		     'transport' => 'postMessage'
    		     )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Icons_Control(
    	    $wp_customize, 
    	        'second_service_icon', 
    	         array(
    	        'type' 		=> 'the_multiple_icons',	                
    	        'label' 	=> esc_html__( 'Second Service Icon', 'the-multiple' ),
    	        'description' 	=> esc_html__( 'Choose the icon from lists.', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_service_setups',
    	            )	            	
    	        )
    	    );
    
     //select post for service
     $wp_customize->add_setting('the_multiple_service_post3',array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'the_multiple_sanitize_number',
        'transport' => 'postMessage'
    ));
   
    $wp_customize->add_control('the_multiple_service_post3', array(
        'type' => 'select',
        'label' => esc_html__('Service Post 3','the-multiple'),
        'section' => 'the_multiple_service_setups',
        'setting' => 'the_multiple_service_post3',
        'choices' => $posts_list
    ));
    
    // service icons two
    $wp_customize->add_setting(
    	     'third_service_icon',
    		    array(
    		    'default' => '',
    		    'sanitize_callback' => 'sanitize_text_field',
    		     'transport' => 'postMessage'
    		     )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Icons_Control(
    	    $wp_customize, 
    	        'third_service_icon', 
    	         array(
    	        'type' 		=> 'the_multiple_icons',	                
    	        'label' 	=> esc_html__( 'Second Service Icon', 'the-multiple' ),
    	        'description' 	=> esc_html__( 'Choose the icon from lists.', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_service_setups',
    	            )	            	
    	        )
    	    );
    //Service Background Image
    $wp_customize->add_setting('the_multiple_service_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_service_bkgimage', array(
		'label' => esc_html__('Background mage for Service', 'the-multiple'),
		'section' => 'the_multiple_service_setups',
		'setting' => 'the_multiple_service_bkgimage'
		))); 
    // starting team section  
    $wp_customize->add_section('the_multiple_team_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Team Section','the-multiple'),
		'description' => esc_html__('Manage Team Service','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
    
     //Enable/Disable Team Section
	$wp_customize->add_setting('the_multiple_homepage_setting_team_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_homepage_setting_team_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_team_setups',
		                'setting'       =>  'the_multiple_homepage_setting_team_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    $wp_customize->add_setting(
	        'the_multiple_team_section_layout',
	        array(
	            'default'           => 'layout-1',
	            'sanitize_callback' => 'sanitize_text_field',
	        )
	    );	    
	$wp_customize->add_control(
	   'the_multiple_team_section_layout',
	            array(
	                'label'    => esc_html__( 'Team Section Layout', 'the-multiple' ),
	                'section'  => 'the_multiple_team_setups',
                    'type' => 'radio',
	                'choices'  => array(
		                    'layout-1' => esc_html__( 'Layout 1', 'the-multiple' ),
		                    'layout-2' => esc_html__( 'Layout 2', 'the-multiple' ),
		            ),
		            'priority' => 6
	            )
	    );
    //select category for team section
   	$wp_customize->add_setting('themultiple_homepage_setting_team_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control(
		            'themultiple_homepage_setting_team_section_category',array(
		                'priority'      =>  20,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'the_multiple_team_setups',
		                'setting'       =>  'themultiple_homepage_setting_team_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            );
    //Service Section Title
    $wp_customize->add_setting(
	                'the_multiple_homepage_setting_team_title_text',array(
	                    'default'           =>  esc_html__('Our team','the-multiple'),
	                    'sanitize_callback' =>  'sanitize_text_field',
	                    'transport'			=>	'postMessage',
	                    )
	                );

	            $wp_customize->add_control(
	                'the_multiple_homepage_setting_team_title_text',array(
	                    'priority'      =>  10,
	                    'label'         =>  esc_html__('Team title text','the-multiple'),
	                    'section'       =>  'the_multiple_team_setups',
	                    'setting'       =>  'the_multiple_homepage_setting_team_title_text',
	                    'type'          =>  'text',  
	                    )                                     
	                );
                
    //Starting Counter Section
	 $wp_customize->add_section('the_multiple_counter_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Counter Section','the-multiple'),
		'description' => esc_html__('Manage Counter Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		)); 

	//Enable/Disble Counter	Section
	$wp_customize->add_setting('the_multiple_homepage_setting_counter_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_homepage_setting_counter_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_counter_setups',
		                'setting'       =>  'the_multiple_homepage_setting_counter_section_option',
		                'priority'      =>  1,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                ));                  
	
	
		 //first counter
	$the_multiple_separator_label = array( 'First', 'Second', 'Third',);
	$the_multiple_default_fact_icon = array( 'fa-coffee', 'fa-rocket', 'fa-code',);
	$the_multile_default_fact_title = array( 'Clients We Have', 'X-Rays Done', 'Live Saved');
	$the_multiple_default_fact_number = array( '798', '237', '54871',);
    	
	$the_multiple_separator_label = array( 'First', 'Second', 'Third',);
	$the_multiple_default_fact_icon = array( 'fa-coffee', 'fa-rocket', 'fa-code',);
	$the_multile_default_fact_title = array( 'Clients We Have', 'X-Rays Done', 'Live Saved');
	$the_multiple_default_fact_number = array( '798', '237', '54871',);
	    
    
    
    	 //first counter
	 $wp_customize->add_setting(
    	    'counter_icon_sec_separator',
    		array(
    		    'default' => '',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'counter_icon_sec_separator', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( ' First  Counter', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_counter_setups',
    	         'priority'  => '2'
    	            )	            	
    	        )
    	    );
     $wp_customize->add_setting(
        'first_counter_title', 
    	array(
    	'default' => 'fa-rocket',
    	    'sanitize_callback' => 'sanitize_text_field',
    	    'transport' => 'postMessage'
    		       )
    	    );    
    $wp_customize->add_control(
    	 'first_counter_title',
    	        array(
    		    'type' => 'text',
    		    'label' => esc_html__( 'Counter Title', 'the-multiple' ),
    		    'section' => 'the_multiple_counter_setups',
    		    'priority' => '3'
    	        )
    	    );
     $wp_customize->add_setting(
    	'first_counter_number', 
    	        array(
    	        'default' => '1243',
    	        'sanitize_callback' => 'the_multiple_sanitize_number',
    	        'transport' => 'postMessage'
    		    )
    	    );    
    $wp_customize->add_control(
    	'first_counter_number',
    	    array(
    		'type' => 'number',
    		'label' => esc_html__( 'counter Number', 'the-multiple' ),
    		'section' => 'the_multiple_counter_setups',
    		   'priority' => '4'
    	            )
    	    );

    $wp_customize->add_setting(
    	     'first_counter_icon',
    		    array(
    		    'default' => 'fa-coffee',
    		    'sanitize_callback' => 'sanitize_text_field',
    		     'transport' => 'postMessage'
    		     )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Icons_Control(
    	    $wp_customize, 
    	        'first_counter_icon', 
    	         array(
    	        'type' 		=> 'the_multiple_icons',	                
    	        'label' 	=> esc_html__( 'counter Icon', 'the-multiple' ),
    	        'description' 	=> esc_html__( 'Choose the icon from lists.', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_counter_setups',
    	          'priority'  => '5',
    	            )	            	
    	        )
    	    );
   
     // second counter
    $wp_customize->add_setting(
    	    'counter_icon_sec_separator_two',
    		array(
    		    'default' => '1534',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'counter_icon_sec_separator_two', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( 'Second  Counter', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_counter_setups',
    	         'priority'  => '6'
    	            )	            	
    	        )
    	    );
    $wp_customize->add_setting(
    'second_counter_title', 
    	array(
    	'default' =>('Clients We Have'),
    	    'sanitize_callback' => 'sanitize_text_field',
    	    'transport' => 'postMessage'
    		       )
    	    );    
    $wp_customize->add_control(
    'second_counter_title',
    	 array(
    		'type' => 'text',
    		'label' => esc_html__( ' Counter Title', 'the-multiple' ),
    		    'section' => 'the_multiple_counter_setups',
    		    'priority' => '7'
    	            )
    	    );
    $wp_customize->add_setting(
    'second_counter_number', 
    	     array(
    	    'default' => '798',
    	     'sanitize_callback' => 'the_multiple_sanitize_number',
    	    'transport' => 'postMessage'
    		       	)
    	    );    
    $wp_customize->add_control(
    'second_counter_number',
    	array(
    		'type' => 'number',
    		 'label' => esc_html__( 'counter Number', 'the-multiple' ),
    		 'section' => 'the_multiple_counter_setups',
    		 'priority' => '8'
    	            )
    	    );
    $wp_customize->add_setting(
    'second_counter_icon',
    	array(
    		 'default' => 'fa-rocket',
    		 'sanitize_callback' => 'sanitize_text_field',
    		 'transport' => 'postMessage'
    		        )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Icons_Control(
    	$wp_customize, 
    	 'second_counter_icon', 
    	     array(
    	      'type' => 'the_multiple_icons',	                
    	        'label'=> esc_html__( 'counter Icon', 'the-multiple' ),
    	        'description' => esc_html__( 'Choose the icon from lists.', 'the-multiple' ),
    	             'section' 	=> 'the_multiple_counter_setups',
    	              'priority'  => '9',
    	            )	            	
    	        )
    	    );
    
    //third counter
    $wp_customize->add_setting(
    	    'counter_icon_sec_separator_three',
    		array(
    		    'default' => '',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'counter_icon_sec_separator_three', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( ' Third Counter', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_counter_setups',
    	         'priority'  => '9'
    	            )	            	
    	        )
    	    );
    $wp_customize->add_setting(
    	 'third_counter_title', 
    	    array(
    	    'default' =>('Live Saved'),
    	    'sanitize_callback' => 'sanitize_text_field',
    	    'transport' => 'postMessage'
    		       )
    	    );    
    $wp_customize->add_control(
    'third_counter_title',
    	   array(
    		'type' => 'text',
    		'label' => esc_html__( '  Counter Title', 'the-multiple' ),
    		    'section' => 'the_multiple_counter_setups',
    		            'priority' => '10'
    	            )
    	    );
    $wp_customize->add_setting(
    	'third_counter_number', 
    	    array(
    	    'default' => '54871',
    	        'sanitize_callback' => 'the_multiple_sanitize_number',
    	         'transport' => 'postMessage'
    		       	)
    	    );    
    $wp_customize->add_control(
    	'third_counter_number',
    	    array(
    		    'type' => 'number',
    		        'label' => esc_html__( 'counter Number', 'the-multiple' ),
    		        'section' => 'the_multiple_counter_setups',
    		        'priority' => '10'
    	            )
    	    );
    $wp_customize->add_setting(
    	'third_counter_icon',
    	array(
    	    'default' => '',
    		 'sanitize_callback' => 'sanitize_text_field',
    		  'transport' => 'postMessage'
    		        )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Icons_Control(
    $wp_customize, 
    	'third_counter_icon', 
    	array(
    	    'type' 		=> 'the_multiple_icons',	                
    	    'label' 	=> esc_html__( 'counter Icon', 'the-multiple' ),
    	    'description' 	=> esc_html__( 'Choose the icon from lists.', 'the-multiple' ),
    	    'section' 	=> 'the_multiple_counter_setups',
    	    'priority'  => '10',
    	          )	            	
    	        )
    	    );

     //Counter Background Image
    $wp_customize->add_setting('the_multiple_counter_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_counter_bkgimage', array(
		'label' => esc_html__('Background Image for Counter', 'the-multiple'),
		'section' => 'the_multiple_counter_setups',
		'setting' => 'the_multiple_counter_bkgimage'
		))); 

    //starting   latest News section
    $wp_customize->add_section('the_multiple_latest_news_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Latest News Section','the-multiple'),
		'description' => esc_html__('Manage Latest Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		)); 
    $wp_customize->add_setting('themultiple_homepage_setting_latest_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('themultiple_homepage_setting_latest_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_latest_news_setups',
		                'setting'       =>  'themultiple_homepage_setting_latest_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    //latest news title
    $wp_customize->add_setting(
	                'the_multiple_homepage_setting_latest_news_section_menu_title_text',array(
	                    'default'           =>  esc_html__('Latest News','the-multiple'),
	                    'sanitize_callback' =>  'sanitize_text_field',
	                    'transport'			=>	'postMessage',
	                    )
	                );

	$wp_customize->add_control(
					'the_multiple_homepage_setting_latest_news_section_menu_title_text',array(
					'priority'      =>  8,
					'label'         =>  esc_html__('Menu title text','the-multiple'),
					'section'       =>  'the_multiple_latest_news_setups',
					'setting'       =>  'the_multiple_homepage_setting_latest_news_section_menu_title_text',
					'type'          =>  'text',  
	                    )   
	                    );                                  
	//select category for latest news section
   	$wp_customize->add_setting('themultiple_homepage_setting_latest_news_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control(
		            'themultiple_homepage_setting_latest_news_section_category',array(
		                'priority'      =>  20,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'the_multiple_latest_news_setups',
		                'setting'       =>  'themultiple_homepage_setting_latest_news_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            ); 
    //starting FAQ Section
     $wp_customize->add_section('the_multiple_faq_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('FAQ Section','the-multiple'),
		'description' => esc_html__('Manage FAQ Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
     // Enable/Disable This Section
	 $wp_customize->add_setting('the_multiple_faq_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_faq_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_faq_setups',
		                'setting'       =>  'the_multiple_faq_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    $wp_customize->add_setting(
    	    'faq_separator_one',
    		array(
    		    'default' => '',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'faq_separator_one', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( 'Left FAQ Portion', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_faq_setups',
    	         'priority'  => '10'
    	            )	            	
    	        )
    	    );

    // FAQ Section title
    $wp_customize->add_setting('the_multiple_faq_title', array(
		'default' => esc_html__('Sometimes you just need a little help','the-multiple'),
		'sanitize_callback' => 'the_multiple_sanitize_text',
		));

	$wp_customize->add_control('the_multiple_faq_title',array(
		'type' => 'text',
		'label' => esc_html__('FAQ Section Left Title','the-multiple'),
		'section' => 'the_multiple_faq_setups',
		'setting' => 'the_multiple_faq_title'
		));	   
	//FAQ section description
    $wp_customize->add_setting('the_multiple_faq_desc', array(
    'default' => '',
        'sanitize_callback' => 'the_multiple_sanitize_text',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control('the_multiple_faq_desc',array(
        'type' => 'textarea',
        'label' => esc_html__('FAQ Section Description','the-multiple'),
        'section' => 'the_multiple_faq_setups',
        'setting' => 'the_multiple_faq_desc'
    )); 
	$wp_customize->add_setting(
    	    'faq_separator_two',
    		array(
    		    'default' => '',
    		            'sanitize_callback' => 'sanitize_text_field',
    		      )
    	    );
    $wp_customize->add_control( new the_multiple_Customize_Section_Separator(
    	    $wp_customize, 
    	    'faq_separator_two', 
    	    array(
    	    'type' 		=> 'the_multiple_separator',
    	        'label' 	=> esc_html__( 'Right FAQ Portion', 'the-multiple' ),
    	        'section' 	=> 'the_multiple_faq_setups',
    	         'priority'  => '10'
    	            )	            	
    	        )
    	    );
    $wp_customize->add_setting('the_multiple_faq_second_title', array(
		'default' =>'',
		'sanitize_callback' => 'the_multiple_sanitize_text',
		));

	$wp_customize->add_control('the_multiple_faq_second_title',array(
		'type' => 'text',
		'label' => esc_html__('FAQ Section Right Title','the-multiple'),
		'section' => 'the_multiple_faq_setups',
		'setting' => 'the_multiple_faq_second_title'
		));
	 //category for the FAQ  section
	 	$wp_customize->add_setting('the_multiple_faq_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control('the_multiple_faq_section_category',array(
		                'priority'      =>  10,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'the_multiple_faq_setups',
		                'setting'       =>  'the_multiple_faq_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            );		   
    //FAQ Background  image
    $wp_customize->add_setting('the_multiple_faq_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_faq_bkgimage', array(
		'label' => esc_html__('Background Image for FAQ Section ', 'the-multiple'),
		'section' => 'the_multiple_faq_setups',
		'setting' => 'the_multiple_faq_bkgimage'
		)));
    //Starting Testimonial Section
     $wp_customize->add_section('the_multiple_testimonial_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Testimonial Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
    // Enable/Disable This Section
	 $wp_customize->add_setting('the_multiple_testimonial_section',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_testimonial_section',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_testimonial_setups',
		                'setting'       =>  'the_multiple_testimonial_section',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
     $wp_customize->add_setting(
	        'the_multiple_testimonial_section_layout',
	        array(
	            'default'           => 'layout-1',
	            'sanitize_callback' => 'sanitize_text_field',
	        )
	    );	    
	$wp_customize->add_control(
	   'the_multiple_testimonial_section_layout',
	            array(
	                'label'    => esc_html__( 'Testimonial  Section Layout', 'the-multiple' ),
	                'section'  => 'the_multiple_testimonial_setups',
                    'type' => 'radio',
	                'choices'  => array(
		                    'layout-1' => esc_html__( 'Layout 1', 'the-multiple' ),
		                    'layout-2' => esc_html__( 'Layout 2', 'the-multiple' ),
		            ),
		            'priority' => 10
	            )
	    );
     //category for the testimonial  section
	 $wp_customize->add_setting('the_multiple_testimonial_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control('the_multiple_testimonial_section_category',array(
		                'priority'      =>  20,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'the_multiple_testimonial_setups',
		                'setting'       =>  'the_multiple_testimonial_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            );
	// testimonial title
    $wp_customize->add_setting('the_multiple_testimonial_homepage_title', array(
		'default' => esc_html__('What People Are Saying About US','the-multiple'),
		'sanitize_callback' => 'the_multiple_sanitize_text',
		));

	$wp_customize->add_control('the_multiple_testimonial_homepage_title',array(
		'type' => 'text',
		'label' => esc_html__('Testimonial Section Title','the-multiple'),
		'section' => 'the_multiple_testimonial_setups',
		'setting' => 'the_multiple_testimonial_homepage_title'
		));	   
	//Testimonial  image
    $wp_customize->add_setting('the_multiple_testimonial_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_testimonial_bkgimage', array(
		'label' => esc_html__('Background Image for Testimonial', 'the-multiple'),
		'section' => 'the_multiple_testimonial_setups',
		 'priority'  => '100',
		'setting' => 'the_multiple_testimonial_bkgimage'
		)));


	//starting blog section
	$wp_customize->add_section('the_multiple_blog_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Blog Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		));
	//enable/disable this section
    $wp_customize->add_setting('the_multiple_home_blog_setting',array(
	'default' => 'yes',
	'sanitize_callback' => 'the_multiple_sanitize_radio_yes_no'
	)
    );
	$wp_customize->add_control('the_multiple_home_blog_setting',array(
		'type' => 'radio',
		'label' => __('Enable/Disable This  Section', 'the-multiple'),
		'description' => __('Do you want to enable this section?','the-multiple'),
		'section' => 'the_multiple_blog_setups',
		'choices' => array(
			'yes' => __('Yes', 'the-multiple'),
			'no' => __('No', 'the-multiple'),
			),
		)
	);
	 $wp_customize->add_setting(
	        'the_multiple_blog_section_layout',
	        array(
	            'default'           => 'layout-1',
	            'sanitize_callback' => 'sanitize_text_field',
	        )
	    );	    
	$wp_customize->add_control(
	   'the_multiple_blog_section_layout',
	            array(
	                'label'    => esc_html__( 'Team Section Layout', 'the-multiple' ),
	                'section'  => 'the_multiple_blog_setups',
                    'type' => 'radio',
	                'choices'  => array(
		                    'layout-1' => esc_html__( 'Layout 1', 'the-multiple' ),
		                    'layout-2' => esc_html__( 'Layout 2', 'the-multiple' ),
		            ),
		            'priority' => 10
	            )
	    );
	$wp_customize->add_setting('the_multiple_home_blog_title',array(
		'default' => '',
		'sanitize_callback' => 'the_multiple_sanitize_text'
		)
	);
	$wp_customize->add_control('the_multiple_home_blog_title',array(
		'type' => 'text',
		'label' => __('Blog Section Title', 'the-multiple'),
		'section' => 'the_multiple_blog_setups',
		)
	);
	//select category for blog
    $wp_customize->add_setting('the_multiple_blog_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control('the_multiple_blog_section_category',array(
		                'priority'      =>  10,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'the_multiple_blog_setups',
		                'setting'       =>  'the_multiple_blog_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                )                                     
		            );
	//blog read more text
    $wp_customize->add_setting(
		            'the_multiple_blog_section_readmore',array(
		                'default'           =>  esc_html__('Know more','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'the_multiple_blog_section_readmore',array(
		                'priority'      =>  10,
		                'label'         =>  esc_html__('Read More Text','the-multiple'),
		                'section'       =>  'the_multiple_blog_setups',
		                'setting'       =>  'the_multiple_blog_section_readmore',
		                'type'          =>  'text',  
		                )                                     
		            );
    //Blog Background  image
    $wp_customize->add_setting('the_multiple_blog_bkgimage', array(
		'default' => '',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
		));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'the_multiple_blog_bkgimage', array(
		'label' => esc_html__('Background Image for Blog Section', 'the-multiple'),
		'section' => 'the_multiple_blog_setups',
		'setting' => 'the_multiple_blog_bkgimage'
		)));

	//Starting Contact Us Section
    $wp_customize->add_section('the_multiple_contact_us_setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Contact Us Section','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		)); 
    //Enable/Disble Our client
	$wp_customize->add_setting('the_multiple_contact_us_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_contact_us_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_contact_us_setups',
		                'setting'       =>  'the_multiple_contact_us_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                ));    
    //select post for Contact to section 
    $wp_customize->add_setting('the_multiple_contact_us_post',array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'the_multiple_sanitize_text',
        'transport' => 'postMessage'
    ));
   
    $wp_customize->add_control('the_multiple_contact_us_post', array(
        'type' => 'select',
        'label' => esc_html__('Contact Us Post','the-multiple'),
        'section' => 'the_multiple_contact_us_setups',
        'setting' => 'the_multiple_contact_us_post',
        'choices' => $posts_list
    ));
    $wp_customize->add_setting(
		            'the_multiple_contact_us_section_link',array(
		                'default'           =>  esc_html__('Contact Us','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'the_multiple_contact_us_section_link',array(
		                'priority'      =>  6,
		                'label'         =>  esc_html__('Fill The Contact Email','the-multiple'),
		                'section'       =>  'the_multiple_contact_us_setups',
		                'setting'       =>  'the_multiple_contact_us_section_link',
		                'type'          =>  'text',  
		                )                                     
		            );
     $wp_customize->add_setting(
		            'the_multiple_contact_section_readmore',array(
		                'default'           =>  esc_html__('Read More','the-multiple'),
		                'sanitize_callback' =>  'sanitize_text_field',
		                )
		            );

    $wp_customize->add_control(
		            'the_multiple_contact_section_readmore',array(
		                'priority'      =>  7,
		                'label'         =>  esc_html__('Fill Read More','the-multiple'),
		                'section'       =>  'the_multiple_contact_us_setups',
		                'setting'       =>  'the_multiple_contact_section_readmore',
		                'type'          =>  'text',  
		                )                                     
		            );	            	            	            

    //starting client section
    $wp_customize->add_section('the_multiple_our_client__setups',array(
		'priority' => '10',
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Our Client Section','the-multiple'),
		'description' => esc_html__('Manage Your Client','the-multiple'),
		'panel' => 'themultiple_homepage_setups'
		)); 
    //Enable/Disble Our client
	$wp_customize->add_setting('the_multiple_homepage_setting_our_client_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_homepage_setting_our_client_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_our_client__setups',
		                'setting'       =>  'the_multiple_homepage_setting_our_client_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                ));                   
		             
    //select category for client section
   	$wp_customize->add_setting('themultiple_homepage_setting_our_client_section_category',array(
		                'default'           =>  '0',
		                'sanitize_callback' =>  'the_multiple_sanitize_category_select',
		                )
		            );
    $wp_customize->add_control(
		            'themultiple_homepage_setting_our_client_section_category',array(
		                'priority'      =>  20,
		                'label'         =>  esc_html__('Select category','the-multiple'),
		                'section'       =>  'the_multiple_our_client__setups',
		                'setting'       =>  'themultiple_homepage_setting_our_client_section_category',
		                'type'          =>  'select',  
		                'choices'       =>  $the_multiple_category_lists
		                ));  
     
		             
    //starting footer section
     $wp_customize -> add_panel(
        'the_multiple_footer_setting_panel',array(
            'priority' => 35,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => esc_html__('Footer Setup', 'the-multiple')
        )
    );
    $wp_customize->add_section(
        'the_multiple_social_link',array(
            'title' =>esc_html__('Footer Social Link','the-multiple'),
            'panel' =>'the_multiple_footer_setting_panel',
        )
    );
    //enble/disable footer social section
    $wp_customize->add_setting('the_multiple_footer_social_section_option',
                    array(
		                'default'           =>  'no',
		                'sanitize_callback' =>  'the_multiple_sanitize_radio_yes_no',
		                )
		            );
    $wp_customize->add_control('the_multiple_footer_social_section_option',array(
		                'description'   =>  esc_html__('Do you want to enable this section?','the-multiple'),
		                'section'       =>  'the_multiple_social_link',
		                'setting'       =>  'the_multiple_footer_social_section_option',
		                'priority'      =>  5,
		                'type'          =>  'radio',
		                'choices'        =>  array(
		                    'yes'   =>  esc_html__('Yes','the-multiple'),
		                    'no'    =>  esc_html__('No','the-multiple')
		                    )
		                )                   
		            );
    $wp_customize->add_setting(
        'the_multiple_facebook_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
    $wp_customize->add_setting(
        'the_multiple_twitter_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
    $wp_customize->add_setting(
        'the_multiple_googleplus_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
    $wp_customize->add_setting(
        'the_multiple_youtube_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
    $wp_customize->add_setting(
        'the_multiple_pinterest_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
   
    $wp_customize->add_setting(
        'the_multiple_linkedin_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
     $wp_customize->add_setting(
        'the_multiple_instagram_text',array(
                'default'=>'',
                'sanitize_callback' => 'esc_url_raw',
            )
    );
   $wp_customize->add_control(
        'the_multiple_facebook_text', array(
                'label' => esc_html__('Facebook Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );
    $wp_customize->add_control(
        'the_multiple_twitter_text',array(
                'label' => esc_html__('Twitter Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );
    $wp_customize->add_control(
        'the_multiple_googleplus_text',array(
                'label' => esc_html__('GooglePlus Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );

    $wp_customize->add_control(
        'the_multiple_youtube_text',array(
                'label' => esc_html__('Youtube Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );
    $wp_customize->add_control(
        'the_multiple_pinterest_text',array(
                'label' => esc_html__('Pinterest Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );
    $wp_customize->add_control(
        'the_multiple_linkedin_text',array(
                'label' => esc_html__('Linkedin Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );
    $wp_customize->add_control(
        'the_multiple_instagram_text',array(
                'label' => esc_html__('Instagram Link','the-multiple'),
                'section' => 'the_multiple_social_link',
                'type' => 'text',
            )
    );
	
    //starting footer copy right section
    $wp_customize->add_section(
        'the_multiple_footer_cpy',array(
            'title' =>esc_html__('Footer Copyright Section','the-multiple'),
            'panel' =>'the_multiple_footer_setting_panel',
        )
    );
    //Starting footer cpy setting
    $wp_customize->add_setting(
	                'the_multiple_footer_setting_cpy_text',array(
	                    'default'           =>  esc_html__('buzthemes','the-multiple'),
	                    'sanitize_callback' =>  'sanitize_text_field',
	                    'transport'			=>	'postMessage',
	                    )
	                );
	$wp_customize->add_control(
					'the_multiple_footer_setting_cpy_text',array(
					'priority'      =>  8,
					'label'         =>  esc_html__('Footer Copyright text','the-multiple'),
					'section'       =>  'the_multiple_footer_cpy',
					'setting'       =>  'the_multiple_footer_setting_cpy_text',
					'type'          =>  'text',  
	                    )   
	                    );  	          

 }
add_action( 'customize_register', 'the_multiple_custom_customize_register' );