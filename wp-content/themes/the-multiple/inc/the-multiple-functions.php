<?php
/**
 * the-multiple functions and definitions
 *
 * @package the-multiple
 */
/**
 * My Functions
 */

/** the-multiple category **/
function the_multiple_category_lists(){
  $category 	=	get_categories();
  $cat_list 	=	array();
  $cat_list[0]=	esc_html__('Select category','the-multiple');
  foreach ($category as $cat) {
     $cat_list[$cat->slug]	=	$cat->name;
  }
 return $cat_list;
}

/**the-multiple Posts List **/
function the_multiple_post_list(){
    $options_posts = array();
    $options_posts_obj = get_posts('posts_per_page=-1');
    $options_posts[''] = esc_html__('Select a Post:','the-multiple');
    foreach ($options_posts_obj as $post) {
        $options_posts[$post->ID] = $post->post_title;
    }
    return $options_posts;
}

// web layout
 function the_multiple_theme_body_classes($classes){
        global $post;
        if(is_home() || is_front_page()){
                
                if(get_theme_mod('the_multiple_weblayout','fullwidth') == 'boxed'){
                  $classes[]= 'boxed-layout';
              }
                elseif(get_theme_mod('the_multiple_weblayout','fullwidth') == 'fullwidth'){
                    $classes[]='fullwidth';
                }
        }
            return $classes;
 }
 add_filter( 'body_class', 'the_multiple_theme_body_classes' );
 
//homepage slider configuration settings
function the_multiple_homepage_slider_content(){
  if(get_theme_mod('themultiple_homepage_setting_slider_section_option','no') == 'yes'):
    ?>
    <div id="home-slider">
      <?php 
      $slider_category = esc_attr(get_theme_mod('themultiple_homepage_setting_slider_section_category'));
      $slider_readmore = esc_html(get_theme_mod('themultiple_setting_slider_section_readmore',esc_html__('Read More','the-multiple')));
      $slider_caption = get_theme_mod('the_multiple_home_slider_caption','caption-left');
      if( !empty($slider_category)) {
        $loop = new WP_Query(
          array(
            'category_name' => $slider_category,
            'posts_per_page' => -1    
            )
          );
          ?>
        <div class="em-slider">
          <div class="main-slider">
           <?php
            if($loop->have_posts()) {
              while($loop->have_posts()) {
                $loop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'the-multiple-slider-image', true );
                ?>
                <div class="slides">
                  <img src="<?php echo esc_url($image[0]); ?>" />
                  <?php
                    ?>
                    <div class="slide-caption <?php echo esc_attr($slider_caption);?>"> 
                    <?php 
                      if($slider_caption != 'caption-no'): ?> 
                            <h2 class="caption-title">
                              <?php the_title();?>
                            </h2> 
                            <div class="slide-desc">
                              <?php echo esc_attr(wp_trim_words(get_the_content(),150,'&hellip;')); ?>
                            </div>       
                            <?php 
                            if(!empty($slider_readmore)){
                            ?>
                               <a href="<?php the_permalink();?>" class="btn more"><?php echo $slider_readmore;?></a>
                              <?php
                            }
                            ?>
                        <?php
                       endif; ?>
                    </div>
                </div>
                <?php 
              }
            }
            ?>
          </div>
        </div>
          <?php   
      }
      ?>
    </div>
      <?php
      endif; 
  }
add_action('the_multiple_homepage_slider','the_multiple_homepage_slider_content');

/** FA icon array **/
function the_multiple_icons_array(){
    $ap_icon_list_raw = 'fa-glass, fa-music, fa-search, fa-envelope-o, fa-heart, fa-star, fa-star-o, fa-user, fa-film, fa-th-large, fa-th, fa-th-list, fa-check, fa-times, fa-search-plus, fa-search-minus, fa-power-off, fa-signal, fa-cog, fa-trash-o, fa-home, fa-file-o, fa-clock-o, fa-road, fa-download, fa-arrow-circle-o-down, fa-arrow-circle-o-up, fa-inbox, fa-play-circle-o, fa-repeat, fa-refresh, fa-list-alt, fa-lock, fa-flag, fa-headphones, fa-volume-off, fa-volume-down, fa-volume-up, fa-qrcode, fa-barcode, fa-tag, fa-tags, fa-book, fa-bookmark, fa-print, fa-camera, fa-font, fa-bold, fa-italic, fa-text-height, fa-text-width, fa-align-left, fa-align-center, fa-align-right, fa-align-justify, fa-list, fa-outdent, fa-indent, fa-video-camera, fa-picture-o, fa-pencil, fa-map-marker, fa-adjust, fa-tint, fa-pencil-square-o, fa-share-square-o, fa-check-square-o, fa-arrows, fa-step-backward, fa-fast-backward, fa-backward, fa-play, fa-pause, fa-stop, fa-forward, fa-fast-forward, fa-step-forward, fa-eject, fa-chevron-left, fa-chevron-right, fa-plus-circle, fa-minus-circle, fa-times-circle, fa-check-circle, fa-question-circle, fa-info-circle, fa-crosshairs, fa-times-circle-o, fa-check-circle-o, fa-ban, fa-arrow-left, fa-arrow-right, fa-arrow-up, fa-arrow-down, fa-share, fa-expand, fa-compress, fa-plus, fa-minus, fa-asterisk, fa-exclamation-circle, fa-gift, fa-leaf, fa-fire, fa-eye, fa-eye-slash, fa-exclamation-triangle, fa-plane, fa-calendar, fa-random, fa-comment, fa-magnet, fa-chevron-up, fa-chevron-down, fa-retweet, fa-shopping-cart, fa-folder, fa-folder-open, fa-arrows-v, fa-arrows-h, fa-bar-chart, fa-twitter-square, fa-facebook-square, fa-camera-retro, fa-key, fa-cogs, fa-comments, fa-thumbs-o-up, fa-thumbs-o-down, fa-star-half, fa-heart-o, fa-sign-out, fa-linkedin-square, fa-thumb-tack, fa-external-link, fa-sign-in, fa-trophy, fa-github-square, fa-upload, fa-lemon-o, fa-phone, fa-square-o, fa-bookmark-o, fa-phone-square, fa-twitter, fa-facebook, fa-github, fa-unlock, fa-credit-card, fa-rss, fa-hdd-o, fa-bullhorn, fa-bell, fa-certificate, fa-hand-o-right, fa-hand-o-left, fa-hand-o-up, fa-hand-o-down, fa-arrow-circle-left, fa-arrow-circle-right, fa-arrow-circle-up, fa-arrow-circle-down, fa-globe, fa-wrench, fa-tasks, fa-filter, fa-briefcase, fa-arrows-alt, fa-users, fa-link, fa-cloud, fa-flask, fa-scissors, fa-files-o, fa-paperclip, fa-floppy-o, fa-square, fa-bars, fa-list-ul, fa-list-ol, fa-strikethrough, fa-underline, fa-table, fa-magic, fa-truck, fa-pinterest, fa-pinterest-square, fa-google-plus-square, fa-google-plus, fa-money, fa-caret-down, fa-caret-up, fa-caret-left, fa-caret-right, fa-columns, fa-sort, fa-sort-desc, fa-sort-asc, fa-envelope, fa-linkedin, fa-undo, fa-gavel, fa-tachometer, fa-comment-o, fa-comments-o, fa-bolt, fa-sitemap, fa-umbrella, fa-clipboard, fa-lightbulb-o, fa-exchange, fa-cloud-download, fa-cloud-upload, fa-user-md, fa-stethoscope, fa-suitcase, fa-bell-o, fa-coffee, fa-cutlery, fa-file-text-o, fa-building-o, fa-hospital-o, fa-ambulance, fa-medkit, fa-fighter-jet, fa-beer, fa-h-square, fa-plus-square, fa-angle-double-left, fa-angle-double-right, fa-angle-double-up, fa-angle-double-down, fa-angle-left, fa-angle-right, fa-angle-up, fa-angle-down, fa-desktop, fa-laptop, fa-tablet, fa-mobile, fa-circle-o, fa-quote-left, fa-quote-right, fa-spinner, fa-circle, fa-reply, fa-github-alt, fa-folder-o, fa-folder-open-o, fa-smile-o, fa-frown-o, fa-meh-o, fa-gamepad, fa-keyboard-o, fa-flag-o, fa-flag-checkered, fa-terminal, fa-code, fa-reply-all, fa-star-half-o, fa-location-arrow, fa-crop, fa-code-fork, fa-chain-broken, fa-question, fa-info, fa-exclamation, fa-superscript, fa-subscript, fa-eraser, fa-puzzle-piece, fa-microphone, fa-microphone-slash, fa-shield, fa-calendar-o, fa-fire-extinguisher, fa-rocket, fa-maxcdn, fa-chevron-circle-left, fa-chevron-circle-right, fa-chevron-circle-up, fa-chevron-circle-down, fa-html5, fa-css3, fa-anchor, fa-unlock-alt, fa-bullseye, fa-ellipsis-h, fa-ellipsis-v, fa-rss-square, fa-play-circle, fa-ticket, fa-minus-square, fa-minus-square-o, fa-level-up, fa-level-down, fa-check-square, fa-pencil-square, fa-external-link-square, fa-share-square, fa-compass, fa-caret-square-o-down, fa-caret-square-o-up, fa-caret-square-o-right, fa-eur, fa-gbp, fa-usd, fa-inr, fa-jpy, fa-rub, fa-krw, fa-btc, fa-file, fa-file-text, fa-sort-alpha-asc, fa-sort-alpha-desc, fa-sort-amount-asc, fa-sort-amount-desc, fa-sort-numeric-asc, fa-sort-numeric-desc, fa-thumbs-up, fa-thumbs-down, fa-youtube-square, fa-youtube, fa-xing, fa-xing-square, fa-youtube-play, fa-dropbox, fa-stack-overflow, fa-instagram, fa-flickr, fa-adn, fa-bitbucket, fa-bitbucket-square, fa-tumblr, fa-tumblr-square, fa-long-arrow-down, fa-long-arrow-up, fa-long-arrow-left, fa-long-arrow-right, fa-apple, fa-windows, fa-android, fa-linux, fa-dribbble, fa-skype, fa-foursquare, fa-trello, fa-female, fa-male, fa-gratipay, fa-sun-o, fa-moon-o, fa-archive, fa-bug, fa-vk, fa-weibo, fa-renren, fa-pagelines, fa-stack-exchange, fa-arrow-circle-o-right, fa-arrow-circle-o-left, fa-caret-square-o-left, fa-dot-circle-o, fa-wheelchair, fa-vimeo-square, fa-try, fa-plus-square-o, fa-space-shuttle, fa-slack, fa-envelope-square, fa-wordpress, fa-openid, fa-university, fa-graduation-cap, fa-yahoo, fa-google, fa-reddit, fa-reddit-square, fa-stumbleupon-circle, fa-stumbleupon, fa-delicious, fa-digg, fa-pied-piper-pp, fa-pied-piper-alt, fa-drupal, fa-joomla, fa-language, fa-fax, fa-building, fa-child, fa-paw, fa-spoon, fa-cube, fa-cubes, fa-behance, fa-behance-square, fa-steam, fa-steam-square, fa-recycle, fa-car, fa-taxi, fa-tree, fa-spotify, fa-deviantart, fa-soundcloud, fa-database, fa-file-pdf-o, fa-file-word-o, fa-file-excel-o, fa-file-powerpoint-o, fa-file-image-o, fa-file-archive-o, fa-file-audio-o, fa-file-video-o, fa-file-code-o, fa-vine, fa-codepen, fa-jsfiddle, fa-life-ring, fa-circle-o-notch, fa-rebel, fa-empire, fa-git-square, fa-git, fa-hacker-news, fa-tencent-weibo, fa-qq, fa-weixin, fa-paper-plane, fa-paper-plane-o, fa-history, fa-circle-thin, fa-header, fa-paragraph, fa-sliders, fa-share-alt, fa-share-alt-square, fa-bomb, fa-futbol-o, fa-tty, fa-binoculars, fa-plug, fa-slideshare, fa-twitch, fa-yelp, fa-newspaper-o, fa-wifi, fa-calculator, fa-paypal, fa-google-wallet, fa-cc-visa, fa-cc-mastercard, fa-cc-discover, fa-cc-amex, fa-cc-paypal, fa-cc-stripe, fa-bell-slash, fa-bell-slash-o, fa-trash, fa-copyright, fa-at, fa-eyedropper, fa-paint-brush, fa-birthday-cake, fa-area-chart, fa-pie-chart, fa-line-chart, fa-lastfm, fa-lastfm-square, fa-toggle-off, fa-toggle-on, fa-bicycle, fa-bus, fa-ioxhost, fa-angellist, fa-cc, fa-ils, fa-meanpath, fa-buysellads, fa-connectdevelop, fa-dashcube, fa-forumbee, fa-leanpub, fa-sellsy, fa-shirtsinbulk, fa-simplybuilt, fa-skyatlas, fa-cart-plus, fa-cart-arrow-down, fa-diamond, fa-ship, fa-user-secret, fa-motorcycle, fa-street-view, fa-heartbeat, fa-venus, fa-mars, fa-mercury, fa-transgender, fa-transgender-alt, fa-venus-double, fa-mars-double, fa-venus-mars, fa-mars-stroke, fa-mars-stroke-v, fa-mars-stroke-h, fa-neuter, fa-genderless, fa-facebook-official, fa-pinterest-p, fa-whatsapp, fa-server, fa-user-plus, fa-user-times, fa-bed, fa-viacoin, fa-train, fa-subway, fa-medium, fa-y-combinator, fa-optin-monster, fa-opencart, fa-expeditedssl, fa-battery-full, fa-battery-three-quarters, fa-battery-half, fa-battery-quarter, fa-battery-empty, fa-mouse-pointer, fa-i-cursor, fa-object-group, fa-object-ungroup, fa-sticky-note, fa-sticky-note-o, fa-cc-jcb, fa-cc-diners-club, fa-clone, fa-balance-scale, fa-hourglass-o, fa-hourglass-start, fa-hourglass-half, fa-hourglass-end, fa-hourglass, fa-hand-rock-o, fa-hand-paper-o, fa-hand-scissors-o, fa-hand-lizard-o, fa-hand-spock-o, fa-hand-pointer-o, fa-hand-peace-o, fa-trademark, fa-registered, fa-creative-commons, fa-gg, fa-gg-circle, fa-tripadvisor, fa-odnoklassniki, fa-odnoklassniki-square, fa-get-pocket, fa-wikipedia-w, fa-safari, fa-chrome, fa-firefox, fa-opera, fa-internet-explorer, fa-television, fa-contao, fa-500px, fa-amazon, fa-calendar-plus-o, fa-calendar-minus-o, fa-calendar-times-o, fa-calendar-check-o, fa-industry, fa-map-pin, fa-map-signs, fa-map-o, fa-map, fa-commenting, fa-commenting-o, fa-houzz, fa-vimeo, fa-black-tie, fa-fonticons, fa-reddit-alien, fa-edge, fa-credit-card-alt, fa-codiepie, fa-modx, fa-fort-awesome, fa-usb, fa-product-hunt, fa-mixcloud, fa-scribd, fa-pause-circle, fa-pause-circle-o, fa-stop-circle, fa-stop-circle-o, fa-shopping-bag, fa-shopping-basket, fa-hashtag, fa-bluetooth, fa-bluetooth-b, fa-percent, fa-gitlab, fa-wpbeginner, fa-wpforms, fa-envira, fa-universal-access, fa-wheelchair-alt, fa-question-circle-o, fa-blind, fa-audio-description, fa-volume-control-phone, fa-braille, fa-assistive-listening-systems, fa-american-sign-language-interpreting, fa-deaf, fa-glide, fa-glide-g, fa-sign-language, fa-low-vision, fa-viadeo, fa-viadeo-square, fa-snapchat, fa-snapchat-ghost, fa-snapchat-square, fa-pied-piper, fa-first-order, fa-yoast, fa-themeisle, fa-google-plus-official, fa-font-awesome' ;
    $ap_icon_list = explode( ", " , $ap_icon_list_raw);
    return $ap_icon_list;
 }

// latest section date and time 
function the_muliple_home_posted_on_cb(){
  global $post;
  $show_comment_count = get_theme_mod('post_option_comment','1');
  $show_post_date = get_theme_mod('post_option_date','1');
  
  $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
  if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
    $time_string_human = human_time_diff(get_the_modified_time( 'U' ),current_time('timestamp')).' '.esc_html__('ago','the-multiple');
    $time_string = '<time class="entry-date published" datetime="%1$s">'.$time_string_human.'</time><time class="updated" datetime="%3$s">%4$s</time>';
    }

  $time_string = sprintf( $time_string,
    esc_attr( get_the_date( 'c' ) ),
    esc_html( get_the_date('M d, Y') ),
    esc_attr( get_the_modified_date( 'c' ) ),
    esc_html( get_the_modified_date('M d, Y') )
    );

  if($show_post_date==1){
    $posted_on = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';
  } else {
    $posted_on = '';
  }
  echo '<span class="posted-on">' . $posted_on . '</span>';
  if($show_comment_count==1){
    $post_comment_count = comments_popup_link( );
  }
}
add_action( 'the_multiple_home_posted_on', 'the_muliple_home_posted_on_cb', 10 );

// function to add social icons
function the_multiple_footer_social_link(){
        $facebooklink =  get_theme_mod('the_multiple_facebook_text');
        $twitterlink =  get_theme_mod('the_multiple_twitter_text');
        $google_pluslink =  get_theme_mod('the_multiple_googleplus_text');
        $youtubelink = get_theme_mod('the_multiple_youtube_text');
        $pinterestlink = get_theme_mod('the_multiple_pinterest_text');
        $linkedinlink = get_theme_mod('the_multiple_linkedin_text');
        $instagramlink =  get_theme_mod('the_multiple_instagram_text');
      ?>
        <div class="social-icons ">
            <?php if(!empty($facebooklink)){?>
                <a href="<?php echo esc_url($facebooklink); ?>" class="facebook" target="_blank"><i class="fa fa-facebook"></i><span></span></a>
                <?php } ?>
                
            <?php if(!empty($twitterlink)){?>
            <a href="<?php echo esc_url($twitterlink); ?>" class="twitter" target="_blank"><i class="fa fa-twitter"></i><span></span></a>
            <?php } ?>

            <?php if(!empty($google_pluslink)){?>
            <a href="<?php echo esc_url($google_pluslink); ?>" class="gplus" target="_blank"><i class="fa fa-google-plus"></i><span></span></a>
            <?php } ?>

            <?php if(!empty($youtubelink)){ ?>
            <a href="<?php echo esc_url($youtubelink); ?>" class="youtube" target="_blank"><i class="fa fa-youtube"></i><span></span></a>
            <?php } ?>

            <?php if(!empty($pinterestlink)){ ?>
            <a href="<?php echo esc_url($pinterestlink); ?>" class="pinterest"  target="_blank"><i class="fa fa-pinterest"></i><span></span></a>
            <?php } ?>

            <?php if(!empty($linkedinlink)){ ?>
            <a href="<?php echo esc_url($linkedinlink); ?>" class="linkedin"target="_blank"><i class="fa fa-linkedin"></i><span></span></a>
            <?php } ?>

            <?php if(!empty($instagramlink)){ ?>
            <a href="<?php echo esc_url($instagramlink); ?>" class="instagram" target="_blank"><i class="fa fa-instagram"></i><span></span></a>
            <?php } ?>

        </div>
        <?php
}
add_action('the_multiple_footer_social_link_action','the_multiple_footer_social_link');

function the_multiple_sanitize_bradcrumb($input){
    $all_tags = array(
        'a'=>array(
            'href'=>array()
        )
     );
    return wp_kses($input,$all_tags);
    
}
// the-multiple breadcrumbs settingg
 if ( ! function_exists( 'the_multiple_breadcrumbs' ) ) :
    function the_multiple_breadcrumbs() {
       global $post;
    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show

    $delimiter = '&gt;';

    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $homeLink = esc_url( home_url() );

    if (is_home() || is_front_page()) {

        if ($showOnHome == 1)
            echo '<div id="the-multiple-breadcrumb" class="the-multiple-breadcrumb"><a href="' . $homeLink . '">' . esc_html__('Home', 'the-multiple') . '</a></div></div>';
    } else {

        echo '<div id="the-multiple-breadcrumb" class="the-multiple-breadcrumb"><a href="' . $homeLink . '">' . esc_html__('Home', 'the-multiple') . '</a> ' . esc_attr($delimiter) . ' ';

        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0)
                echo get_category_parents($thisCat->parent, TRUE, ' ' . esc_attr($delimiter) . ' ');
            echo '<span class="current">' . esc_html__('Archive by category','the-multiple').' "' . single_cat_title('', false) . '"' . '</span>';
        } elseif (is_search()) {
            echo '<span class="current">' . esc_html__('Search results for','the-multiple'). '"' . get_search_query() . '"' . '</span>';
        } elseif (is_day()) {
            echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_attr(get_the_time('Y')) . '</a> ' . esc_attr($delimiter) . ' ';
            echo '<a href="' . esc_url(get_month_link(get_the_time('Y')), esc_attr(get_the_time('m'))) . '">' . esc_attr(get_the_time('F')) . '</a> ' . esc_attr($delimiter) . ' ';
            echo '<span class="current">' . esc_attr(get_the_time('d')) . '</span>';
        } elseif (is_month()) {
            echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_attr(get_the_time('Y')) . '</a> ' . esc_attr($delimiter) . ' ';
            echo '<span class="current">' . esc_attr(get_the_time('F')) . '</span>';
        } elseif (is_year()) {
            echo '<span class="current">' . esc_attr(get_the_time('Y')) . '</span>';
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . esc_url($homeLink) . '/' . esc_attr($slug['slug']) . '/">' . esc_attr($post_type->labels->singular_name) . '</a>';
                if ($showCurrent == 1)
                    echo ' ' . esc_attr($delimiter) . ' ' . '<span class="current">' . esc_attr(get_the_title()) . '</span>';
            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                if ($showCurrent == 0)
                    $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                echo the_multiple_sanitize_bradcrumb($cats);
                if ($showCurrent == 1)
                    echo '<span class="current">' . esc_attr(get_the_title()) . '</span>';
            }
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            if($post_type){
            echo '<span class="current">' . esc_attr($post_type->labels->singular_name) . '</span>';
            }
        } elseif (is_attachment()) {
            if ($showCurrent == 1) echo ' ' . '<span class="current">' . esc_attr(get_the_title()) . '</span>';
        } elseif (is_page() && !$post->post_parent) {
            if ($showCurrent == 1)
                echo '<span class="current">' . esc_attr(get_the_title()) . '</span>';
        } elseif (is_page() && $post->post_parent) {
            $parent_id = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo the_multiple_sanitize_bradcrumb($breadcrumbs[$i]);
                if ($i != count($breadcrumbs) - 1)
                    echo ' ' . esc_attr($delimiter). ' ';
            }
            if ($showCurrent == 1)
                echo ' ' . esc_attr($delimiter) . ' ' . '<span class="current">' . esc_attr(get_the_title()) . '</span>';
        } elseif (is_tag()) {
            echo '<span class="current">' . esc_html__('Posts tagged','the-multiple').' "' . esc_attr(single_tag_title('', false)) . '"' . '</span>';
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo '<span class="current">' . esc_html__('Articles posted by ','the-multiple'). esc_attr($userdata->display_name) . '</span>';
        } elseif (is_404()) {
            echo '<span class="current">' . esc_html__('Error 404','the-multiple') . '</span>';
        }

        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                echo ' (';
            echo esc_html__('Page', 'the-multiple') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                echo ')';
        }

        echo '</div>';
    }
    }
endif;


 if ( ! function_exists( 'the_multiple_page_title' ) ) :
    function the_multiple_page_title(){
    ?>
        <header class="page-header">
                <div class="header-banner" data-stellar-background-ratio="0.5" style="background-image: url('<?php echo esc_url(get_theme_mod('the_multiple_page_bg_image','')); ?>');">
                <div class="banner-title">
                        <?php
                  if(is_archive()) {
                    the_archive_title( '<h1 class="page-title">', '</h1>' );
                    the_archive_description( '<div class="taxonomy-description">', '</div>' );
                  } elseif(is_single() || is_singular('page')) {
                    wp_reset_postdata();
                    the_title('<h1 class="page-title">', '</h1>');
                  } elseif(is_search()) {
                                ?>
                                <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'the-multiple' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                                <?php
                            } elseif(is_404()) {
                                ?>
                                <h1 class="page-title"><?php esc_html_e( '404 Error', 'the-multiple' ); ?></h1>
                                <?php
                            }
                ?>
                        <?php the_multiple_breadcrumbs(); ?>
                    </div>
                </div>
        </header>
    <?php
    }
endif;
add_action('the_multiple_title','the_multiple_page_title');


