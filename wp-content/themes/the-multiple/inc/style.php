<?php
/**
 * @package the-multiple
 */
/** Dynamic css function **/
function the_multiple_dynamic_style(){
$the_multiple_theme_color = esc_attr(get_theme_mod('the_multiple_skim_color'));
$the_multiple_style_css = '';
   if($the_multiple_theme_color){
   	$the_multiple_style_css .= ".site-footer-info h3:after, .the-multiple-counter .icon-holder:hover, .the-multiple-latest-news-section .latest-news-context .block-poston a:hover .widget_text input[type='submit'], .site-footer .widget_text input[type='submit']:hover{border-color:$the_multiple_theme_color}";

   	$the_multiple_style_css .= " .layout-1 .blog-context{border-color:$the_multiple_theme_color}";
   	$the_multiple_style_css .= " .tagcloud a:hover::after, .layout-1 .blog-context{border-left-color:$the_multiple_theme_color}";


    $the_multiple_style_css .= "h2.widget-title:before
   ,.the-multiple-counter .col i:hover,#wp-calendar tfoot a,.icon,a.btn.more, a.btn.more, .form-submit .submit,.the-multiple-mail-link .btn,.the-multiple-mail-link .btn.btn-alter:hover:after ,.layout-2 .team-caption-title, .the-multiple-testimonial-section.layout-2 .section-title.form-submit .submit,.sidebar .widget .widget-title,.header-search .search-submit, .sidebar .search-submit, .error-404-section .search-submit, .search-form .search-submit,.tagcloud a:hover, .service-post .service-icon i, .wrap-law-post-right .view-more, #ed-top ,a.btn.more, .form-submit .wrap-law-post-right .view-more, a.html5lightbox:hover{background:$the_multiple_theme_color}";

      $the_multiple_style_css .= ".site-text a h1,.call-to-one i, .widget_the_multiple_recent_posts .recent-post-content a.recent-post-title-widget:hover, .layout-3  a.btn.more, .the-multiple-breadcrumb a,  .layout-1 .blog-context .blog-caption-title a:hover, .layout-1 .blog-context .block-poston a:hover, h2.section-title,.left-side h2, .the-multiple-blog-section.layout-2 .block-poston a:hover, .layout-2 .feature-title a:hover, .feature-title a:hover, .the-multiple-blog-section.layout-2 .blog-caption-title a:hover, .layout-2 .testimonial-caption-title, .right-side h2, .the-multiple-blog-section .section-title, .layout-2 .testimonial-caption-title::before, .the-multiple-testimonial-section.layout-2 .section-title, .call-to-two i,.sidebar .widget ul li a:hover,.comments-area a,.site-main .post a, .site-main .page a,.content-area .entry-header .entry-title,.site-footer-info .widget_recent_entries ul li a:hover,.site-footer-info ul li a:hover:before, .widget ul li:hover:before,.site-footer-info ul li a:hover, .widget ul li a:hover ,.copyright a:hover, .latest-news-caption-title a:hover,.service-post .service-title a:hover,.the-multiple-latest-news-section .latest-news-context .block-poston a:hover, .team_the_multiple .team-caption .team-caption-title, .feature-title a .service-post .service-title a:hover,.site-text a h1 #primary .entry-meta > span a:hover,.entry-header .entry-meta a{color:$the_multiple_theme_color}";

   }
    wp_add_inline_style( 'the-multiple-style', $the_multiple_style_css );

}
add_action('wp_enqueue_scripts','the_multiple_dynamic_style');	

