<?php
       if( class_exists( 'WP_Customize_Control' ) ):
       
        /**  The Multiple Pro Link **/
        class the_multiple_Link_Section extends WP_Customize_Section {
    
            public $type = 'the-multiple-pro';
    
            public $pro_text = '';
    
            public $pro_url = '';
    
            public function json() {
                $json = parent::json();
                $json['pro_text'] = $this->pro_text;
                $json['pro_url']  = esc_url( $this->pro_url );
                return $json;
            }
            protected function render_template() { ?>
    
                <li id="custom-section-{{ data.id }}" class="custom-section control-section control-section-{{ data.type }} cannot-expand">
                    <h3 class="custom-section-title">
                        {{ data.title }}
                        <# if ( data.pro_text && data.pro_url ) { #>
                            <a href="{{ data.pro_url }}" class="button button-custom alignright" target="_blank">{{ data.pro_text }}</a>
                        <# } #>
                    </h3>
                </li>
            <?php }
        }
        
        /**
     * Theme info
     */
    class the_multiple_Theme_Info extends WP_Customize_Control {
        public function render_content(){

            $our_theme_infos = array(
                'demo' => array(
                   'link' => esc_url( 'https://buzthemes.com/demo/the-multiple/' ),
                   'text' => esc_html__( 'View Demo', 'the-multiple' ),
                ),
                'documentation' => array(
                   'link' => esc_url( 'https://buzthemes.com/doc/the-multiple' ),
                   'text' => esc_html__( 'Documentation', 'the-multiple' ),
                ),
                'support' => array(
                   'link' => esc_url( 'https://buzthemes.com/forums/forum/the-multiple' ),
                   'text' => esc_html__( 'Support', 'the-multiple' ),
                ),
            );
            foreach ( $our_theme_infos as $our_theme_info ) {
                echo '<p><a target="_blank" href="' . $our_theme_info['link'] . '" >' . esc_html( $our_theme_info['text'] ) . ' </a></p>';
            }
        ?>
        	<label>
        	    <h2 class="customize-title"><?php echo esc_html( $this->label ); ?></h2>
        	    <span class="customize-text_editor_desc">                 
        	        <ul class="admin-pro-feature-list">   
                        <li><span><?php esc_html_e('One Click Demo Import','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Modern and elegant design','the-multiple'); ?> </span></li>
                        <li><span><?php esc_html_e('3 Homepage Layouts','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('100% Responsive theme','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Advanced Typography','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Breadcrumb Settings','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Highly configurable home page','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Four Footer Widget Areas','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Sidebar Options','the-multiple'); ?> </span></li>
        	            <li><span><?php esc_html_e('Translation ready','the-multiple'); ?> </span></li>
                        <li><span><?php esc_html_e('WordPress Live Customizer Based','the-multiple'); ?> </span></li>
        	        </ul>
        	        <?php $the_multiple_pro_link = 'https://buzthemes.com/wordpress_themes/the-multiple-pro-multipurpose-theme/'; ?>
        	        <a href="<?php echo esc_url($the_multiple_pro_link); ?>" class="button button-primary buynow" target="_blank"><?php esc_html_e('Buy Now','the-multiple'); ?></a>
        	    </span>
        	</label>
        <?php
        }
    }
    
      
    endif;