<?php
/**
* Custom Sanitizer Function 
*/

function the_multiple_sanitize_radio_yes_no($input){
        $option = array(
                'yes'   =>  esc_html__('Yes','the-multiple'),
                'no'    =>  esc_html__('No','the-multiple')
            );
        if(array_key_exists($input, $option)){
            return $input;
        }
        else
            return '';
    }

function the_multiple_sanitize_category_select($input){
    $the_multiple_category_lists = the_multiple_category_lists();
    if(array_key_exists($input,$the_multiple_category_lists)){
        return $input;
    }else{
        return '';
    }
}

// sanitizer for call to action
function the_multiple_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

// the-multiple number sanitizer
function the_multiple_sanitize_number( $input ) {
    $output = intval($input);
     return $output;
}

//logo alignment
function the_multiple_radio_sanitize_alignment_logo($input) {
      $valid_keys = array(
        'left'=>esc_html__('Left', 'the-multiple'),
        'center'=>esc_html__('Center', 'the-multiple'),
      );
      if ( array_key_exists( $input, $valid_keys ) ) {
         return $input;
      } else {
         return '';
      }
   }
   
function the_multiple_radio_sanitize_webpagelayout($input) {
    $valid_keys = array(
        'fullwidth' => esc_html__('Full Width', 'the-multiple'),
        'boxed' => esc_html__('Boxed', 'the-multiple')
    );
    if ( array_key_exists( $input, $valid_keys ) ) {
        return $input;
    } else {
        return '';
    }
}   
