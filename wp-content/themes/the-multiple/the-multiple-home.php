<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Home Page
 * @package the-multiple
 */

get_header(); ?>

<div id="primary" class="content-area">
	 <main id="main" class="site-main" role="main">
        <?php

            /** Feature Section **/
            get_template_part( 'template-parts/sections/section', 'feature' );

            /** About Section **/
            get_template_part( 'template-parts/sections/section', 'about' );

            /** Book And Appointment Section **/
            get_template_part( 'template-parts/sections/section', 'bookapp' );

            /** Service Section **/
            get_template_part( 'template-parts/sections/section', 'service' );

            /** Service Section **/
            get_template_part( 'template-parts/sections/section', 'team' );

            /** Counter Section **/
            get_template_part( 'template-parts/sections/section', 'counter' );

            /** Latest News Section **/
            get_template_part( 'template-parts/sections/section', 'latest-news');

             /** Testimonial  Section **/
            get_template_part( 'template-parts/sections/section', 'faq');

            /** Testimonial  Section **/
            get_template_part( 'template-parts/sections/section', 'testimonial');

            /** Testimonial  Section **/
            get_template_part( 'template-parts/sections/section', 'blog');

             /** Client Section **/
            get_template_part( 'template-parts/sections/section', 'contact-us'); 

            /** Client Section **/
            get_template_part( 'template-parts/sections/section', 'client'); 

        ?>
	 </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();?>
