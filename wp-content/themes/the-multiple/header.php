<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package the-multiple
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
   <?php  $the_multiple_header_layout = get_theme_mod('the_multiple_header_layout','layout-1');?> 
    <?php  $bt_nav = get_theme_mod('the_multiple_logo_alignment','left');?> 
    <?php if($the_multiple_header_layout != 'layout-3' ){ ?>
<div id="page" class="site">
	    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'the-multiple' ); ?></a>
	      <?php  
               $bt_logo_alignment = '';
                $bt_nav_alignment = '';                              
                if($bt_nav=='left'){
                    $bt_logo_alignment = 'logo-left';
                    $bt_nav_alignment = 'menu-left';
                } 
                else{
                    $bt_logo_alignment = 'logo-center';
                    $bt_nav_nav_alignment = 'menu-center';
                } }
            ?>   
    <?php if($the_multiple_header_layout == 'layout-3' ){ ?>          
   <?php  $bt_logo_alignment = get_theme_mod('the_multiple_logo_alignment','left');?>
   <?php } ?>             
	<header id="masthead" class="site-header <?php echo esc_attr($bt_logo_alignment);?>  <?php echo esc_attr($the_multiple_header_layout); ?>" role="banner">
	    <div class="bt-container">
           <div class="header-top">
                  <div class="site-branding">
                      <?php if ( function_exists( 'the_custom_logo' ) ): ?>
                           <div class="site-logo">
                             <?php if ( has_custom_logo() ) : ?>
                              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                              <?php the_custom_logo(); ?>
                                </a>
                                  <?php endif; // End logo check. ?>
                            </div>
                            <?php endif; // end custom logo?>
                            
                            <div class="site-text">
                                 <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                    <h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
                                    <p class="site-description"><?php bloginfo( 'description' ); ?></p>
                                 </a>
                             </div>
                     </div><!-- .site-branding -->
                      <?php if($the_multiple_header_layout != 'layout-3' ){ ?>
                      <div class="call-to-one">
                          <?php
                          //call to us
                          $header_callto = get_theme_mod('the_multiple_header_setting_callto_text_one');
                          echo wp_kses_post($header_callto);
                          ?>
                          <?php } ?>
                      </div>
                       <?php if($the_multiple_header_layout != 'layout-3' ){ ?>
                      <div class="call-to-two">
                          <?php
                          //call to us
                          $header_callto = get_theme_mod('the_multiple_header_setting_callto_text_two');
                          echo wp_kses_post($header_callto);
                          ?>
                          <?php } ?>
                      </div>
                   
            </div>
       </div>
    		<nav id="site-navigation" class="main-navigation">
    		    <div class="bt-container">
        		       <label for="toggle" class="icon">
                        <span class="ham"> </span>
                    </label>
        				<input type="checkbox" class="menu-toggle" id="toggle" aria-controls="primary-menu" aria-expanded="false">
        				<?php
        					wp_nav_menu( array(
        						'theme_location' => 'menu-1',
        						'menu_id'        => 'primary-menu',
        					) );
        				?>
      				  <div class="header_social_search_wrap clearfix">
      	              <?php if(get_theme_mod('the_multiple_general_search_top_header')!='no'){ ?>
      	                  <div class="header-search ">
      	                      <i class="fa fa-search"></i>
      	                        <?php get_search_form();?>
      	                   </div>
      	                 <?php } ?> 
      	       </div>
    	     </div>                    
    		</nav><!-- #site-navigation -->  
	</header><!-- #masthead -->
<?php 
if(is_home() || is_front_page()){
do_action('the_multiple_homepage_slider');
} ?>
<!-- Page breadcrumbs -->
<?php 
if(!is_home() && !is_front_page()){
    do_action('the_multiple_title'); 
}
?>
	