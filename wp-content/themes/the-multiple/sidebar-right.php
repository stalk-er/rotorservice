<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package the-multiple
 */
global $post;
$post_id = "";
if(is_front_page()){
	$post_id = get_option('page_on_front');
}else{
	if($post)
	$post_id = $post->ID;
}
$post_class = get_post_meta( $post_id, 'the_multiple_sidebar_layout', true );
if(empty($post_class) && is_archive()){
	$post_class = "sidebar-right";
}elseif(is_single() || is_search()){
	$post_class = "sidebar-right";
}
if($post_class=='sidebar-right' || $post_class=='sidebar-both'){
    ?>
    <div id="secondary-right" class="widget-area sidebar-both sidebar">
        <?php if ( is_active_sidebar( 'the-multiple-sidebar-right' ) ) : ?>
			<?php dynamic_sidebar( 'the-multiple-sidebar-right' ); ?>
		<?php endif; ?>
    </div>
    <?php    
}    
?>