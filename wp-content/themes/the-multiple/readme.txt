=== The Multiple ===

Contributors: Buzthemes

== Description ==
The Multiple is a responsive free WordPress theme for Multiprupose. All our options use the WordPress Customizer so you can see every change you make live. The theme is also clean, and SEO optimized and we promise to make best website.

The Multiple WordPress Theme, Copyright 2017 The Multiple
The Multiple is distributed under the terms of the GNU GPL v2
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A starter theme called The Multiple, or underscores.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Documentation ==
1. Create page and assign home page template. 
2. Go to setting->Reading->Front page displays->A static page (select below) and choose home page that we have assign as home page template.
3. Appearance->customizer->Home Page Setting here you can add home contents. 

Third Party scripts and styles under public license.
= Styles =	
	Font Awesome v4.6.3 under (Font: SIL OFL 1.1, CSS: MIT License)
	http://fontawesome.io

= Scripts =

    Underscores
    License: [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
    URL: http://underscores.me/, 
    (C) 2012-2016 Automattic, Inc.

    OWL Carousel
    License: MIT License
    URL: https://github.com/OwlCarousel2/OwlCarousel2 
    
    Counter Up
	jQuery counterup v1.0 by Benjamin Intal under GPL v2 Licens.e
	https://github.com/bfintal/Counter-Up
    
    Scroll To
	jQuery ScrollTo v2.1.1 by Ariel Flesler under MIT License
	https://github.com/flesler/jquery.scrollTo
    
    WOW
    https://github.com/matthieua/WOW
    Copyright (c) 2016 Matthieu Aussaguel

    Animate.css
    License: MIT license
    URL: http://daneden.me/animate
    Copyright (c) 2015 Daniel Eden

    * Stellar - Copyright 2014, Mark Dalgleish - http://markdalgleish.com/projects/stellar.js
    License: MIT (http://opensource.org/licenses/MIT)


== Images == 

All images used in the theme designed by buzthemes under GPL License

Image used in screenshot
https://pixabay.com/en/nurse-medicine-doctor-hospital-2019420/
