<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package the-multiple
 */

if ( ! is_active_sidebar( 'the-multiple-sidebar' ) ) {
	return;
}
?>
<aside id="secondary-right" class="widget-area sidebar-right sidebar">
	<?php dynamic_sidebar( 'the-multiple-sidebar' ); ?>
</aside><!-- #secondary -->
